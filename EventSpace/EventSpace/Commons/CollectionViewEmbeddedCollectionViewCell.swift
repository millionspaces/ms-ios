//
//  CollectionViewEmbeddedCollectionViewCell.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 9/12/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class CollectionViewEmbeddedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    func setEmbeddedCollectionViewDatasourceDelegate <D: UICollectionViewDataSource & UICollectionViewDelegate> (dataSourceDelegate: D, identifier id: Int) {
        self.collectionView.dataSource = dataSourceDelegate //as? UICollectionViewDataSource
        self.collectionView.delegate = dataSourceDelegate //as? UICollectionViewDelegate
        self.collectionView.tag = id
        self.collectionView.allowsMultipleSelection = false
        self.collectionView.reloadData()
    }
    
}
