//
//  BookingExtraAmenity.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 10/3/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class BookingExtraAmenity: NSObject {
    
    var index: Int!         // IndexPath Row value for position
    
    var extraAmenity: SpaceExtraAmenity!
    var type: Int!          // Per Hour = 1 | Other = 2
    var amount: Int!
    var totalChargeForExtraAmenity: Int!
    
    
    func initBookingExtraAmenity(withExtraAmenity extra: SpaceExtraAmenity, type: Int, amount: Int, totalCharge: Int, index: Int) {
        self.index = index
        self.extraAmenity = extra
        self.type = type
        self.amount = amount
        self.totalChargeForExtraAmenity = totalCharge
    }
}
