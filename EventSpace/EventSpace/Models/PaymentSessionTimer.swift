//
//  PaymentSessionTimer.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 12/27/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class PaymentSessionTimer: NSObject {

    static let sharedTimer: PaymentSessionTimer = {
        let timer = PaymentSessionTimer()
        return timer
    }()
    
    var internalTimer: Timer?
    
    var time = 300
    
    func startPaymentSessionTimer() {
        if self.internalTimer == nil {
            self.internalTimer?.invalidate()
        }
        
        self.time = 300
        self.internalTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.monitorPaymentSessionTimer), userInfo: nil, repeats: true)
        
        RunLoop.main.add(self.internalTimer!, forMode: RunLoopMode.commonModes)
    }
    
    func resumePaymentSessionTimer() {
        if self.internalTimer == nil {
            self.internalTimer?.invalidate()
        }
        
        self.internalTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.monitorPaymentSessionTimer), userInfo: nil, repeats: true)
    }
    
    func stopPaymentSessionTimer() {
        guard self.internalTimer != nil else {
            print("PaymentSessionTimer stopPaymentSessionTimer | fatalError --> Start PaymentSessionTimer before invalidating")
            return
        }
    
        self.internalTimer?.invalidate()
    }
    
    
    func monitorPaymentSessionTimer() {
        if self.time < 1 {
            self.internalTimer?.invalidate()
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.PAYMEMT_SESSION_IS_UP), object: nil)
            print("PaymentSessionTimer monitorPaymentSessionTimer | PaymentSessionTimer is up")
        }
        else {
            self.time -= 1
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constants.PAYMEMT_SESSION_IN_PROGRESS), object: nil, userInfo: ["timer" : "\(self.timerString(time: self.time))"])
            //print("PaymentSessionTimer monitorPaymentSessionTimer | timer : \(self.timerString(time: self.time))")
        }
    }
    
    private func timerString(time: Int) -> String {
        let minutes = time / 60 % 60
        let seconds = time % 60
        
        return String(format: "%02i:%02i", minutes, seconds)
    }
}
