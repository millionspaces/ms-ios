//
//  SpaceMenu.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 11/13/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SpaceMenu: NSObject {

    var id: Int!
    var menuName: String! // Test
    var menuImage: String!
    
    func initSpaceMenu(withDictionary dic: [String: Any]) {
        self.id = dic["id"] as! Int
        self.menuName = "Menu \(dic["menuId"] as! String)"
        self.menuImage = dic["url"] as! String
    }
}

