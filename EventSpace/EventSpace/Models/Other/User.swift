//
//  User.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 10/9/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class User: NSObject {
    
    // MARK: - Non-optional values
    var id: Int!
    var name: String!
    var email: String!
    
    // MARK: - Optional values
    var profileImageUrl: URL?
    var mobileNumber: String?
    var companyName: String?
    var companyAddress: String?
    var jobTitle: String?
    var companyPhone: String?

    

    func initUser(withUserDetailsDictionary dic: [String : Any]) {
        
        self.id = dic["id"] as! Int
        self.name = dic["name"] as! String
        self.email = dic["email"] as! String
        
        if let imageUrlStr = dic["imageUrl"] as? String {
            if let imgUrl = URL(string: imageUrlStr) {
                self.profileImageUrl = imgUrl
            }
        }
        
        if let mobileNum = dic["mobileNumber"] as? String {
            self.mobileNumber = mobileNum
        }
        
        if let compName = dic["companyName"] as? String {
            self.companyName = compName
        }
        
        if let compAddress = dic["address"] as? String {
            self.companyAddress = compAddress
        }
        
        if let job = dic["jobTitle"] as? String {
            self.jobTitle = job
        }
        
        if let compPhone = dic["companyPhone"] as? String {
            self.companyPhone = compPhone
        }
    }

}
