//
//  SpaceCalendarBooking.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 12/21/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SpaceCalendarBooking: NSObject {

    var id: Int!
    var isManual: Bool!
    
    // From Date and Time
    var bookingFromDate: Date!
    var bookingFromTimeHourInt: Int!  // In 24 Hours
    
    // To Date and Time
    var bookingToDate: Date!
    var bookingToTimeHourInt: Int!
    
    
    func initSpaceCalendarBooking(bookingId: Int, fromDate: Date, fromTimeHourInt: Int, toDate: Date, toTimeHourInt: Int, isManual: Int) {
        self.id = bookingId
        self.isManual = Bool(isManual as NSNumber)
        
        self.bookingFromDate = fromDate
        self.bookingFromTimeHourInt = fromTimeHourInt
        
        self.bookingToDate = toDate
        self.bookingToTimeHourInt = toTimeHourInt
    }
    
}
