//
//  Space.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 6/13/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class Space: NSObject {
    
    var id: Int! // m id
    var name: String! // m name
    var sDescription: String! // m description
    var addressLine1: String! // m addressLine1
    var addressLine2: String? // o addressLine2
    var longitude: Double!  // m longitude
    var latitude: Double!  // m latitude
    
    var participantCount: Int! // m participantCount
    var size: Int! // m size
    var sizeMeasurementUnit: MeasurementUnit! //measurementUnit
    
    var thumbnailImage: String!  // m thumbnailImage
    var images: NSMutableArray = []  // images
    
    var eventTypes: NSMutableArray = [] // eventType  // TODO
    var amenities: NSMutableArray = [] // amenity  // TODO
    var extraAmenities: NSMutableArray = [] //extraAmenity  // TODO
    
    var ratePerHour: Int!  // m ratePerHour
    var availabilityMethod: String! //m     availabilityMethod HOUR_BASE BLOCK_BASE
    var availabilityModules: NSMutableArray = [] //o  availability  // TODO
    var menus: [SpaceMenu] = []
    
    var blockChargeType: Int = 0 // o      if availabilityMethod == BLOCK_BASE ---> 1 = Space Only Blocks | 2 = Per Guest Based Blocks
    var minimumParticipantCount: Int = 0    // Will only have a value when blockChargeType = 2 aka Per Guest Based Blocks
    
    var noticePeriod: Int?  // o noticePeriod
    var bufferTime: Int!    // bufferTime
    var calendarStartDate: String! // m calendarStart
    var calendarEndDate: String!  // m calendarEnd
    var calendarEventSize: Int!     // calendarEventSize
    
    var cancellationPolicy: CancellationPolicy!  // m cancellationPolicy
    var spaceRules: NSMutableArray = [] //o rules  // TODO
    var seatingArrangements: NSMutableArray = [] //seatingArrangements  // TODO
    
    var rating: Float!  // m rating
    var reviewDetails: NSMutableArray = [] // reviewDetails  // TODO
    
    var similarSpaces: NSMutableArray = [] //similarSpaces  // TODO

    var securityDeposit: String?    //securityDeposit // TODO To be Assigned
    
    var createdAt: Int! // createdAt
    var updatedAt: Int! // updatedAt
    
    var hostId: Int!  // m user
    var hostDetails: NSDictionary? //o spaceOwnerDto
    
    // Custom variables
    var fullAdrress: String!
    var hasReimbursableBlocks: Bool = false // Temp Var
    
    var spaceType: SpaceType?
    
    
    func initSpace(withDictionary dic: [String : Any]) {
        self.id = dic["id"] as! Int
        self.name = (dic["name"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        self.sDescription = (dic["description"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        self.addressLine1 = (dic["addressLine1"] as! String).replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
        
        if let address2 = dic["addressLine2"] as? String {
            self.addressLine2 = address2.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil)
            self.fullAdrress = "\(self.addressLine2!), \(self.addressLine1!)"
        }
        else {
            self.fullAdrress = self.addressLine1!
        }
        
        self.longitude = Double(dic["longitude"] as! String)
        self.latitude = Double(dic["latitude"] as! String)
        
        self.participantCount = dic["participantCount"] as! Int
        self.size = dic["size"] as! Int
        
        let measurementUnit = dic["measurementUnit"] as! [String : Any]
        self.sizeMeasurementUnit = MetaData.retrieveMeasurementUnit(with: measurementUnit["id"] as! Int)
        
        self.thumbnailImage = dic["thumbnailImage"] as! String
        self.images.addObjects(from: dic["images"] as! [String])
        
        let eventTypeArr = dic["eventType"] as! [Any]
        for i in eventTypeArr {
            let et = MetaData.retrieveEventType(with: i as! Int)
            self.eventTypes.add(et)
        }
        
        let amenityArr = dic["amenity"] as! [Any]
        for i in amenityArr {
            let a = MetaData.retrieveAmenity(with: i as! Int)
            self.amenities.add(a)
        }
        
        let extrAmenityArr = dic["extraAmenity"] as! [Any]
        for d in extrAmenityArr {
            let ea = SpaceExtraAmenity()
            ea.initSpaceExtraAmenity(withDictionary: d as! [String : Any])
            self.extraAmenities.add(ea)
        }
        
        self.ratePerHour = dic["ratePerHour"] as! Int
        self.availabilityMethod = dic["availabilityMethod"] as! String
        
        let availabilityArr = dic["availability"] as! [NSDictionary]
        //print("Space initSpace | availabilityArr - \(availabilityArr)")

        for d in availabilityArr {
            let am = SpaceAvailabilityModule()
            am.initSpaceAvailabilityModule(withDictionary: d as! [String : Any])
            self.availabilityModules.add(am)
        }
        
        if let menus = dic["menuFiles"] as? [[String : Any]] {
            for i in menus {
                let menu = SpaceMenu()
                menu.initSpaceMenu(withDictionary: i)
                self.menus.append(menu)
            }
            
            if self.menus.count > 0 { self.menus.sort(by: { ($0 as SpaceMenu).menuName < ($1 as SpaceMenu).menuName }) }
        }
        
        if self.availabilityMethod == "BLOCK_BASE" {
            if let blkChargeType = dic["blockChargeType"] as? Int {
                self.blockChargeType = blkChargeType
            }
            
            if self.blockChargeType == 1 { // Space Only
                for a in self.availabilityModules as! [SpaceAvailabilityModule] {
                    if a.isReimbursable {
                        self.hasReimbursableBlocks = true
                        break
                    }
                }
            }
            else if self.blockChargeType == 2 { // Per Guest
                if let minParticipantCount = dic["minParticipantCount"] as? Int {
                    //print("Space initSpace | minParticipantCount - \(minParticipantCount)")
                    self.minimumParticipantCount = minParticipantCount
                }
            }
        }
        
        if !(dic["noticePeriod"] is NSNull) {
            self.noticePeriod = dic["noticePeriod"] as? Int
        }
        
        //self.bufferTime = dic["bufferTime"] as! Int // TEMP:
        if !(dic["bufferTime"] is NSNull) {
            self.bufferTime = dic["bufferTime"] as? Int
        }
        else {
            self.bufferTime = 0
        }
        
        self.calendarStartDate = dic["calendarStart"] as! String
        self.calendarEndDate = dic["calendarEnd"] as! String
        self.calendarEventSize = Int(dic["calendarEventSize"] as! String)
        
        self.cancellationPolicy = MetaData.retrieveCancellationPolicies(with: dic["cancellationPolicy"] as! Int)
        
        let rulesArr = dic["rules"] as! [Any]
        for i in rulesArr {
            let r = MetaData.retrieveSpaceRule(with: i as! Int)
            self.spaceRules.add(r)
        }
        
        let seatingArrangementsArr = dic["seatingArrangements"] as! [Any]
        for d in seatingArrangementsArr {
            let sa = SpaceSeatingArrangement()
            sa.initSpaceSeatingArrangement(withDictionary: d as! [String : Any])
            self.seatingArrangements.add(sa)
        }
        
        self.rating = dic["rating"] as! Float // TODO Clarify
        
        // ReviewDetails // TODO To be Assigned
        if !(dic["reviewDetails"] is NSNull) {
            //print("Space() reviewDetails NOT NULL")
        }
        
        /*
        if !(dic["similarSpaces"] is NSNull) {
            let similarSpacesArr = dic["similarSpaces"] as! [Any]
            for d in similarSpacesArr {
                let ss = Space()
                ss.initSpace(withDictionary: d as! [String : Any])
                self.similarSpaces.add(ss)
            }
        }*/
    
        //securityDeposit // TODO To be Assigned
        if !(dic["securityDeposit"] is NSNull) {
            //print("Space() securityDeposit NOT NULL")
        }
        
        self.createdAt = dic["createdAt"] as! Int
        self.updatedAt = dic["updatedAt"] as! Int
        
        self.hostId = dic["user"] as! Int
        
        //securityDeposit // TODO To be Assigned
        if !(dic["spaceOwnerDto"] is NSNull) {
            //print("Space() spaceOwnerDto NOT NULL")
        }
        
        if let sTypes =  dic["spaceType"] as? [[String : Any]] {
            if sTypes.count > 0 {
                let sType = SpaceType()
                sType.initSpaceType(withDictionary: sTypes[0])
                self.spaceType = sType
            }
        }

    }
    
    func addSimilarSpaces(withSimilarSpacesArray arr: [[String : Any]]) {
        for dic in arr {
            let ss = Space()
            ss.initSpace(withDictionary: dic)
            self.similarSpaces.add(ss)
        }
    }
 }
