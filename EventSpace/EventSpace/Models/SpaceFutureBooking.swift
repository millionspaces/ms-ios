//
//  SpaceFutureBooking.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 9/19/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SpaceFutureBooking: NSObject {
    
    // ==================================================================================================
    // =================================== NOT IN USE SINCE 2017-12-21 ==================================
    // ==================================================================================================
    
    var id: Int!
    var isManual: Bool!

    // From Date and Time
    var bookingFromDate: Date!
    var bookingFromTimeHourInt: Int!  // In 24 Hours
    
    // To Date and Time
    var bookingToDate: Date!
    var bookingToTimeHourInt: Int!
    
    
    func initFutureBooking(bookingId: Int, fromDate: Date, fromTimeHourInt: Int, toDate: Date, toTimeHourInt: Int, isManual: Int) {
        self.id = bookingId
        self.isManual = Bool(isManual as NSNumber)
        
        self.bookingFromDate = fromDate
        self.bookingFromTimeHourInt = fromTimeHourInt
        
        self.bookingToDate = toDate
        self.bookingToTimeHourInt = toTimeHourInt
    }
}
