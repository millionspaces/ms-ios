//
//  SpaceSeatingArrangement.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 8/17/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SpaceSeatingArrangement: NSObject {
    
    var id: Int!
    var arrangement: SeatingArrangement!
    var participantCount: Int!
    
    
    func initSpaceSeatingArrangement(withDictionary dic: [String : Any]) {
        self.id = dic["id"] as! Int
        self.arrangement = MetaData.retrieveSeatingArrangement(with: dic["id"] as! Int)
        self.participantCount = dic["participantCount"] as! Int
    }
}
