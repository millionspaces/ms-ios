//
//  StaticData.swift
//  EventSpace
//
//  Created by Randika Swaris on 4/28/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import CoreData

class StaticData: NSObject {
    
    static var cancellationPolicies:NSMutableDictionary = NSMutableDictionary()
    static var eventTypes:NSMutableDictionary = NSMutableDictionary()
    static var amnities:NSMutableDictionary = NSMutableDictionary()
    
    public static func setUp() {
        setCancellationPolicies()
        setEventTypes()
        setAmtities()
    }
    
    private static func setCancellationPolicies() {
        cancellationPolicies.setValue("Full refund upto two days prior to event, except processing fees", forKey: String(1))
        cancellationPolicies.setValue("Full refund upto five days prior to event, except processing fees", forKey: String(2))
        cancellationPolicies.setValue("50% refund up to one week prior to arrival, except processing fees", forKey: String(3))
    }
    
    
    private static func setEventTypes() {
        eventTypes.setValue("ico-party", forKey: String(1))
        eventTypes.setValue("ico-wedding", forKey: String(2))
        eventTypes.setValue("ico-press", forKey: String(3))
        eventTypes.setValue("ico-meeting", forKey: String(4))
        eventTypes.setValue("ico-interview", forKey: String(5))
        eventTypes.setValue("ico-training", forKey: String(6))
        eventTypes.setValue("ico-photoshoot", forKey: String(7))
    }
    
    private static func setAmtities() {
        amnities.setValue("ico-microphone", forKey: String(1))
        amnities.setValue("ico-screen", forKey: String(2))
        amnities.setValue("ico-projector", forKey: String(3))
        amnities.setValue("ico-lunch", forKey: String(4))
        amnities.setValue("ico-dinner", forKey: String(5))
        amnities.setValue("ico-coffee", forKey: String(6))
        amnities.setValue("ico-snacks", forKey: String(7))
        amnities.setValue("ico-water", forKey: String(8))
        amnities.setValue("ico-stationary", forKey: String(9))
    
    }
    
    public static func getCancellationPolicyText(forID id:Int) -> String {
        if let policyText = cancellationPolicies[String(id)] as? String{
            return policyText
        }
        return "Cancellation Policy Not Defined"
    }
    
    public static func getEventType(forID id:Int)-> String {
        if let eventTypeText = eventTypes[String(id)] as? String {
            return eventTypeText
        }
        return "ico-party"
    }
    
    public static func getEventTypeString(forID id:Int)-> String {
        if let eventTypeText = eventTypes[String(id)] as? String{
            return eventTypeText.replacingOccurrences(of: "ico-", with: "")
        }
        return "Party"
    }
    
    public static func getAmnityType(forID id:Int)-> String {
        if let amnityTypeText = amnities[String(id)] as? String{
            return amnityTypeText
        }
        return "ico-screen"
    }
    
    
    //    static var cancellationPolicies:NSMutableDictionary = NSMutableDictionary()
    
    //    public static func setUp(){
    //    setCancellationPolices ()
    //    print(getCancellationPolicyText(forPolicyId: 1))
    //    }
    //
    //    private static func setCancellationPolices () {
    //        setCancellationPolicies(id: 1, name: "Flexible", description: "Full refund upto two days prior to event, except processing fees", refundRate: 1)
    //        setCancellationPolicies(id: 2, name: "Moderate", description: "Full refund upto five days prior to event, except processing fees", refundRate: 1)
    //        setCancellationPolicies(id: 3, name: "Strict", description: "50% refund up to one week prior to arrival, except processing fees", refundRate: 0.5)
    //    }
    //
    //    private static func setCancellationPolicies (id:Int, name: String, description:String, refundRate:Decimal) {
    //        let policy: CancellationPolicy =
    //
    //
    //        policy.setValue(id, forKey: "1")
    //        policy.setValue(name, forKey: name)
    //        policy.setValue(refundRate, forKey: String(describing: refundRate))
    //        cancellationPolicies.setValue(policy, forKey: String(id))
    //    }
    //
    //    public static func getCancellationPolicyText(forPolicyId policyId:Int) -> String{
    //        if let cancellationPolicy = cancellationPolicies[String(policyId)] as? CancellationPolicy, let policyText = cancellationPolicy.policyDescription {
    //            return policyText
    //        }
    //        return "Cancellation Policy Not Defined"
    //    }
}
