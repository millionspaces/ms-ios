//
//  WebServiceCall.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 6/13/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

typealias SuccessBlock = (Any) -> Void
typealias OldFailureBlock = (Any) -> Void
typealias FailureBlock = (_ errorCode: Int, _ error: String) -> Void

class WebServiceCall: NSObject {
    
    private static let NETWORK_REQUEST_TYPES = (G: "GET", P: "POST", U: "PUT", D: "DELETE")
    private static let RESPONSE_STRUCTURE = (A: "Array", D: "Dictonary", O: "Other_for_testing")

    
    // =========================================== Authentication ==============================================
    
    // POST - Authenticate User via Email/Username and Password
    static func authenticateUser(username: String, password: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        
        let authenticateData = NSMutableData(data: "username=\(username)".data(using: String.Encoding.utf8)!)
        authenticateData.append("&password=\(password)".data(using: String.Encoding.utf8)!)
        
        performUserAuthenticationWebServiceRequest(paramData: authenticateData as Data!, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // POST - Authenticate User with Facebook AccessToken
    static func authenticateUser(withFacebookAccessToken accessToken: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        let facebookAccessData = NSData(data: "username=&password=&facebookToken=\(accessToken)".data(using: String.Encoding.utf8)!)
        performUserAuthenticationWebServiceRequest(paramData: facebookAccessData as Data!, successBlock: successBlock, failureBlock: failureBlock)
    }

    
    // POST - Authenticate User with Google AccessToken
    static func authenticateUser(withGoogleAccessToken accessToken: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        let googleAccessData = NSData(data: "username=&password=&googleToken=\(accessToken)".data(using: String.Encoding.utf8)!)
        performUserAuthenticationWebServiceRequest(paramData: googleAccessData as Data!, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // Common Request for User Authentication
    private static func performUserAuthenticationWebServiceRequest(paramData: Data!, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        
        let session = URLSession.shared
        
        let url = URL(string: Configuration.MS_AUTHENTICATE)
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = NETWORK_REQUEST_TYPES.P
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = paramData
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let data: Data = data, let response: URLResponse = response, error == nil else {
                print("WebServiceCall performUserAuthenticationWebServiceRequest | error : \(String(describing: error!))")
                print("WebServiceCall performUserAuthenticationWebServiceRequest | error code : \((error! as NSError).code)")
                
                // Handling Network unavailability
                if (error! as NSError).code == Constants.ERROR_CODE_INTERNET_OFFLINE || (error! as NSError).code == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    failureBlock(Constants.ERROR_CODE_INTERNET_OFFLINE, AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else if (error! as NSError).code == Constants.ERROR_CODE_REQUEST_TIMEOUT {
                    // TEMP: TODO:
                    failureBlock(Constants.STATUS_CODE_SERVER_GATEWAY_TIMEOUT, AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else {
                    failureBlock(0, AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
                return
            }
            
            let responseStatusCode = (response as! HTTPURLResponse).statusCode
            print("WebServiceCall performUserAuthenticationWebServiceRequest | response status code : \(responseStatusCode)")
            
            if responseStatusCode == Constants.STATUS_CODE_REQUEST_SUCCESS {
                
                AuthenticationManager.setAuthenticationCookies(withAuthenticationResponse: response)
                
                do {
                    let result = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary) ?? NSDictionary()
                    successBlock(result)
                }
                catch {
                    print("WebServiceCall performUserAuthenticationWebServiceRequest | Error In Json response De-serialization")
                    failureBlock(0, AlertMessages.RESPONSE_DE_SERIALIZATION_ERROR_MESSAGE)
                }
            }
            else {
                failureBlock(responseStatusCode, "Some Error")
            }
        }
        task.resume()
    }
    
    
    
    
    // ======================================= Functional Requests ========================================
    
    // Base function for Web Service Request
    private static func performWebServiceRequest(with url: URL, methodType: String, paramData: Data!, requestOptions: [[String]]!, responseStructure: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        //print("WebServiceCall performWebServiceRequest")
        print("WebServiceCall performWebServiceRequest | request url : \(url)")
        
        // TODO: Check Reachability
        
        //let config = URLSessionConfiguration.default
        //let session = URLSession(configuration: config)
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = methodType // As in "POST", "GET", "PUT" or "DELETE"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        if paramData != nil {
            request.httpBody = paramData
        }
        
        if requestOptions != nil {
            for ro in requestOptions {
                request.addValue(ro[1], forHTTPHeaderField: ro[0])
            }
        }
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let data: Data = data, let response: URLResponse = response, error == nil else {
                print("WebServiceCall performWebServiceRequest | error : \(String(describing: error!))")
                print("WebServiceCall performWebServiceRequest | error code : \((error! as NSError).code)")
                
                // Handling Network unavailability
                if (error! as NSError).code == Constants.ERROR_CODE_INTERNET_OFFLINE || (error! as NSError).code == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    failureBlock(Constants.ERROR_CODE_INTERNET_OFFLINE, AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else if (error! as NSError).code == Constants.ERROR_CODE_REQUEST_TIMEOUT {
                    // TEMP: TODO:
                    failureBlock(Constants.STATUS_CODE_SERVER_GATEWAY_TIMEOUT, AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else {
                    failureBlock(0, AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
                return
            }
            
            let responseStatusCode = (response as! HTTPURLResponse).statusCode
            print("WebServiceCall webServiceRequestOther | response status code : \(responseStatusCode)")
            //print("WebServiceCall webServiceRequestOther | response : \(response)")
            //print("WebServiceCall webServiceRequestOther | response url : \(response.url!)")
            //print("WebServiceCall webServiceRequestOther | data : \(data)")
            
            if Constants.STATUS_CODE_REQUEST_SUCCESS_RANGE.contains(responseStatusCode) {
                if responseStructure == RESPONSE_STRUCTURE.A {
                    // JSON De-Serialization if anticipated response data structure is Array
                    do {
                        let result = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray) ?? []
                        successBlock(result)
                    }
                    catch {
                        print("WebServiceCall webServiceRequestOther | Error In Json response De-serialization - Array")
                        failureBlock(0, AlertMessages.RESPONSE_DE_SERIALIZATION_ERROR_MESSAGE)
                    }
                }
                else if responseStructure == RESPONSE_STRUCTURE.D {
                    // JSON De-Serialization if anticipated response data structure is Dictionary
                    do {
                        let result = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary) ?? NSDictionary()
                        successBlock(result)
                    }
                    catch {
                        print("WebServiceCall webServiceRequestOther | Error In Json response De-serialization - Dictionary")
                        failureBlock(0, AlertMessages.RESPONSE_DE_SERIALIZATION_ERROR_MESSAGE)
                    }
                }
                else {
                    if let resultString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                        successBlock(resultString)
                    }
                    else {
                        print("WebServiceCall webServiceRequestOther | Error In Json response De-serialization - Other")
                        failureBlock(0, AlertMessages.RESPONSE_DE_SERIALIZATION_ERROR_MESSAGE)
                    }
                }
            }
            else {
                failureBlock(responseStatusCode, "Some Error")
            }
        }
        task.resume()
    }
    
    
    // =========================================== User ==============================================
    
    // POST - Create / Register User 
    static func registerUser(userDetails: [String : String], successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        do {
            let paramData = try JSONSerialization.data(withJSONObject: userDetails, options: [])
            let url = URL(string: Configuration.USER)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    // PUT - Update User
    static func updateUser(userDetails: [String : String], successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        do {
            let paramData = try JSONSerialization.data(withJSONObject: userDetails, options: [])
            let url = URL(string: Configuration.USER)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.U, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    // POST - Check Email
    static func checkEmail(email: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        let url = URL(string: Configuration.CHECK_EMAIL)
        let requestOptions: [[String]] = [["Content-Type", "application/x-www-form-urlencoded"]]
        let paramData = "email=\(email)".data(using: String.Encoding.utf8)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET - User Details
    static func retrieveUser(successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        let url = URL(string: Configuration.USER)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // ============================================== SPACES =================================================
    
    // GET For All Event Spaces
    static func retrieveAllSpaces(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.SPACES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    static func retrieveSpaces(withPageNumber page: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        do {
            let paramDic = FilterManager.retrieveFiltersForWebRequest()
            let paramData = try JSONSerialization.data(withJSONObject: paramDic, options: [])
            
            let url = URL(string: Configuration.ADVANCE_SEARCH_SPACES + "\(page)")
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
        
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    // GET Space with SpaceId
    static func retrieveSpace(spaceId: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        let url = URL(string: Configuration.SPACES + "/\(spaceId)")

        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Space with SpaceId and Space Event Types for similar spaces
    static func retrieveSpaceWithEventTypes(spaceId: Int, eventTypeIDs: [Int], successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {

        var eventTypeIdsString: String = ""
        for i in eventTypeIDs {
            eventTypeIdsString.append("\(i),")
        }
        
        let url = URL(string: Configuration.SPACES + "/\(spaceId)?eventId=\(String(eventTypeIdsString)!)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Similar Spaces with Space ID and Event Types
    static func retrieveSimilarSpacesForSpace(withSpaceID spaceID: Int, eventTypeIDs: [Int], successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        var eventTypeIdsString: String = ""
        for i in eventTypeIDs {
        eventTypeIdsString.append("\(i),")
        }
        
        let url = URL(string: Configuration.SIMILAR_SPACES + "\(spaceID)?eventId=\(String(eventTypeIdsString)!)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET - My Spaces
    static func retrieveMySpaces(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.MY_SPACES)
    
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }

    // GET - Guest Calendar Details
    static func retrieveGuestCalendarDetails(withSpaceID spaceID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.GUEST_CALENDAR_DETAILS + "\(spaceID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET - Host Calendar Details
    static func retrieveHostCalendarDetails(withSpaceID spaceID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.HOST_CALENDAR_DETAILS + "\(spaceID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // ============================================== BOOKINGS =================================================
    
    // POST - Verify Promo Code
    static func verifyPromoCode(withSpaceID spaceID: Int, promoCode: String, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let parameters = ["space" : "\(spaceID)", "promoCode" : "\(promoCode)"]
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let url = URL(string: Configuration.VERIFY_PROMO_CODE)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    
    }
    
    // POST Book Space
    static func bookSpace(bookingDetails: NSDictionary, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: bookingDetails, options: [])
            let url = URL(string: Configuration.BOOK_SPACE)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    // PUT Tentatively Reserve Space
    static func makeTentativeReservation(withBookingID bookingID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
    
        let tentativeReservationDetails = ["booking_id" : "\(bookingID)", "event" : "1", "method" : "MANUAL", "status" : "1"]
    
        do {
            let paramData = try JSONSerialization.data(withJSONObject: tentativeReservationDetails, options: [])
            let url = URL(string: Configuration.BOOK_SPACE)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.U, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // PUT Cancel Guest MillionSpaces Booking
    static func cancelGuestMSBooking(forBookingID bookingID: Int, withRefundableAmount amount: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        let cancellationDetails = ["booking_id" : "\(bookingID)", "event" : "3", "refund" : "\(amount)", "status" : "1"]
         print("WebServiceCall cancelGuestMSBooking | param dic : \(cancellationDetails)")
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: cancellationDetails, options: [])
            let url = URL(string: Configuration.BOOK_SPACE)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.U, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // GET - My Space Bookings
    static func retrieveMySpaceBookings(forMySpace mySpaceID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.BOOKINGS_MY_SPACE + "\(mySpaceID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    

    // GET - My Activities
    static func retrieveMyActivities(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.BOOKINGS_MY_ACTIVITIES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // POST - Get Booking Details by Booking ID - For Booking Details Screen | Both Guest and Manual Bookings
    static func retrieveBookingDetails(withBookingID bookingId: Int, isManual: Bool, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let parameters = ["bookingId" : "\(bookingId)", "isManual" : "\(isManual)"]
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: parameters, options: [])
            let url = URL(string: Configuration.BOOKING_BY_ID)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // POST - Add Booking Review
    static func addBookingReview(withBookingID bookingID: Int, title: String, rate: String, reviewDescription: String, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        let review = ["bookingId" : "\(bookingID)", "title" : "\(title)", "rate" : "\(rate)", "description": "\(reviewDescription)", "createdAt" : "\(CalendarUtils.currentDateTime()[3])"]
        print("WebServiceCall addBookingReview | param dic : \(review)")
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: review, options: [])
            let url = URL(string: Configuration.ADD_BOOKING_REVIEW)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // PUT - Add Remark to Booking
    // NOTE: Not implemented / Tested
    static func addBookingRemark(withBookingID bookingID: Int, isManual: Bool, remark: String, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        
        let remarkDetails = ["bookingId" : "\(bookingID)", "isManual" : "\(isManual)", "remark" : "\(remark)"]
        
        do {
            let paramData = try JSONSerialization.data(withJSONObject: remarkDetails, options: [])
            let url = URL(string: Configuration.ADD_BOOKING_REMARK)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.U, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    static func retrieveRefundAmount(forBookingID bookingID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.REFUND_FOR_BOOKING_CANCELLATION + "\(bookingID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // ============================================== MANUAL BOOKINGS =================================================
    
    // POST - Manual Booking
    static func addManualBooking(withBookingDetails bookingDetails: [String : Any], successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        do {
            let paramData = try JSONSerialization.data(withJSONObject: bookingDetails, options: [])
            let url = URL(string: Configuration.MANUAL_BOOKING)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            // TEMP - responseStructure
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // PUT - Update Manual Booking
    // NOTE: Not implemented / Tested
    static func updateManualBooking(withBookingDetails bookingDetails: [String : Any], successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        do {
            let paramData = try JSONSerialization.data(withJSONObject: bookingDetails, options: [])
            let url = URL(string: Configuration.MANUAL_BOOKING)
            let requestOptions: [[String]] = [["Content-Type", "application/json"]]
            
            // TEMP - responseStructure
            performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.U, paramData: paramData, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
        }
        catch {
            failureBlock(0, AlertMessages.REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE)
        }
    }
    
    
    // GET - Manual Booking
    // NOTE: Not implemented / Tested
    static func retrieveManualBooking(withBookingID bookingID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.MANUAL_BOOKING + "/\(bookingID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.D, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // DELETE - Manual Booking
    // NOTE: Not implemented / Tested
    static func deleteManualBooking(withBookingID bookingID: Int, successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.MANUAL_BOOKING + "/\(bookingID)")
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.D, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    
    // ========================================= COMMONS / META DATA ============================================
    
    // GET Amenities
    static func allAmenities(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.ALL_AMENITIES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Amenity Units
    static func amenityUnits(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.AMENITY_UNITS)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Event Types
    static func allEventTypes(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string:Configuration.ALL_EVENT_TYPES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Cancellation Policies
    static func allCancellationPolicies(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.CANCELLATION_POLICIES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Space Rules
    static func allSpaceRules(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.SPACE_RULES)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Seating Arrangements
    static func allSeatingArrangements(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.SEATING_ARRANGEMENTS)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    
    // GET Measurement Units
    static func measurementUnits(successBlock:@escaping SuccessBlock, failureBlock:@escaping FailureBlock) {
        let url = URL(string: Configuration.MEASUREMENT_UNITS)
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.G, paramData: nil, requestOptions: nil, responseStructure: RESPONSE_STRUCTURE.A, successBlock: successBlock, failureBlock: failureBlock)
    }
    

    
    // ========================================================= IPG ============================================================
    
    // POST Payment - IPG
    static func ipg(bookingId: String, amount: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        
        let url = URL(string: Configuration.IPG)
        print("WebServiceCall ipg | url : \(url!)")
        
        let keyForSignature = Configuration.PASSWORD + Configuration.MerID + Configuration.AcqID + Configuration.SENTRYORD + bookingId + amount + Configuration.PurchaseCurrency
        
        print("WebServiceCall ipg | key : \(keyForSignature)")
        
        let signature = Utils.prepareIpgSignature(input: keyForSignature)
        print("WebServiceCall ipg | signature : \(signature)")
        
        let postData = NSMutableData(data: "AcqID=\(Configuration.AcqID)".data(using: String.Encoding.utf8)!)
        postData.append("&CaptureFlag=\(Configuration.CaptureFlag)".data(using: String.Encoding.utf8)!)
        postData.append("&MerID=\(Configuration.MerID)".data(using: String.Encoding.utf8)!)
        postData.append("&MerRespURL=\(Configuration.IPG_CALLBACK)".data(using: String.Encoding.utf8)!)
        postData.append("&OrderID=\(Configuration.SENTRYORD+bookingId)".data(using: String.Encoding.utf8)!)
        postData.append("&PurchaseAmt=\(amount)".data(using: String.Encoding.utf8)!)
        postData.append("&PurchaseCurrency=\(Configuration.PurchaseCurrency)".data(using: String.Encoding.utf8)!)
        postData.append("&PurchaseCurrencyExponent=\(Configuration.PurchaseCurrencyExponent)".data(using: String.Encoding.utf8)!)
        postData.append("&Signature=\(signature)".data(using: String.Encoding.utf8)!)
        postData.append("&SignatureMethod=\(Configuration.SignatureMethod)".data(using: String.Encoding.utf8)!)
        postData.append("&Version=\(Configuration.Version)".data(using: String.Encoding.utf8)!)
        
        let requestOptions: [[String]] = [["Content-Type", "application/x-www-form-urlencoded"]]
        
        performWebServiceRequest(with: url!, methodType: NETWORK_REQUEST_TYPES.P, paramData: postData as Data!, requestOptions: requestOptions, responseStructure: RESPONSE_STRUCTURE.O, successBlock: successBlock, failureBlock: failureBlock)
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // Older Base function for Web Service Request
    /*
    private static func performWebServiceRequest(with url: URL, methodType: String, paramString: String!, responseStructure: String, successBlock: @escaping SuccessBlock, failureBlock: @escaping FailureBlock) {
        //print("WebServiceCall performWebServiceRequest")
        
        // TODO: Check Reachability
        
        //let config = URLSessionConfiguration.default
        //let session = URLSession(configuration: config)
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = methodType // As in "POST", "GET", "PUT" or "DELETE"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        //request.httpShouldHandleCookies = true
        
        if paramString != nil {
            request.httpBody = paramString.data(using: String.Encoding.utf8)
        }
        
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                print("WebServiceCall webServiceRequestOther | error : \(String(describing: error))")
                
                // TODO : Handle Network unavailability
                
                failureBlock("Failure")
                return
            }
            
            // JSON De-Serialization if anticipated response data structure is Array
            if responseStructure == RESPONSE_STRUCTURE.A {
                do{
                    if let data = data {
                        let result = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray) ?? []
                        
                        //print("WebServiceCall webServiceRequestOther | deserialized result : \(result)")
                        
                        successBlock(result)
                    }
                }
                catch {
                    print("WebServiceCall webServiceRequestOther | Error In Json De-serialization Response Array")
                    failureBlock("Failure-Other")
                }
            }
            else if responseStructure == RESPONSE_STRUCTURE.D {
                // JSON De-Serialization if anticipated response data structure is Dictionary
                do{
                    if let data = data {
                        let result = try (JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary) ?? NSDictionary()
                        
                        //print("WebServiceCall webServiceRequestOther | deserialized result : \(result)")
                        successBlock(result)
                    }
                }
                catch {
                    print("WebServiceCall webServiceRequestOther | Error In Json De-serialization Response Dictionary")
                    failureBlock("Failure-Other")
                }
            }
            else{
                if let resultString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) {
                    print("WebServiceCall webServiceRequestOther | deserialized result : \(String(describing: resultString))")
                    
                    successBlock(resultString)
                }
                else{
                    print("WebServiceCall webServiceRequestOther | Error In Json De-serialization Response - Other")
                    failureBlock("Failure-Other")
                }
                
            }
        }
        task.resume()
    }*/
    
    

    
    
    /*
     var sharedSession: URLSession?
     
     func initSharedSession() -> WebServiceCall {
     let config = URLSessionConfiguration.default
     sharedSession = URLSession(configuration: config)
     return sharedSession as! WebServiceCall
     }
     */
    
    /*
     private static func webServiceRequest(withURL url:URL){
     print("WebServiceCall webServiceRequest | URL : \(url)")
     
     let config = URLSessionConfiguration.default
     let sharedSession = URLSession(configuration: config)
     
     let task = sharedSession.dataTask(with: url, completionHandler: { (data, response, error) in
     if error != nil {
     print("WebServiceCall webServiceRequest | error : \(error!.localizedDescription)")
     } else {
     print("WebServiceCall webServiceRequest | data : \(String(describing: data))") // JSON Serialization
     }
     })
     
     task.resume()
     }*/
}




