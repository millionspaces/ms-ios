//
//  AlertMessages.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 10/25/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class AlertMessages: NSObject {
    
    // MARK: - Alert Action Titles
    static let OK_ACTION = "OK"
    static let YES_ACTION = "Yes"
    static let NO_ACTION = "No"
    static let DISMISS_ACTION = "Dismiss"
    static let RELOAD_ACTION = "Reload"
    static let RETRY_ACTION = "Retry"
    static let CALL_ACTION = "Call"
    static let GO_BACK_ACTION = "Go Back"
    static let CANCEL_ACTION = "Cancel"
    static let APPLY_ACTION = "Apply"
    
    // MARK: - Alert Titles
    static let SUCCESS_TITLE = "Success"
    static let ERROR_TITLE = "Error"
    static let ALERT_TITLE = "Alert"
    static let WARNING_TITLE = "Warning"
    static let CONFIRM_TITLE = "Confirm"
    static let HI_TITLE = "Hi"
    static let HELLO_TITLE = "Hello"
    static let NOTICE_PERIOD_TITLE = "Short Notice Bookings"
    static let MANUAL_PAYMENT_OPTIONS_DISABLED_TITLE = "Manual Payment Options Disabled"
    static let PROMO_CODE_TITLE = "Apply Promo Code"
    
    
    // MARK: - Alert Messages
    
    // Common
    static let INTERNET_OFFLINE_MESSAGE = "Internet connection appears to be offline. Reload to try again when connection is available!"
    static let INTERNET_OFFLINE_MESSAGE_2 = "Internet connection appears to be offline. Try again when connection is available!"
    static let SERVER_ERROR_MESSAGE = "We are experiencing a server error at the moment. Reload to try again!"
    static let SERVER_ERROR_MESSAGE_2 = "We are experiencing a server error at the moment. Try again!"
    static let INTERNAL_ERROR = "We are experiencing an internal error at the moment."
    
    static let REQUEST_JSON_BODY_SERIALIZATION_ERROR_MESSAGE = "Experienced an error in JSON request body serialization!"
    static let RESPONSE_DE_SERIALIZATION_ERROR_MESSAGE = "Experienced an error in JSON response de-serialization!"
    static let UNKNOWN_ERROR_MESSAGE = "Some unexpected error occured!"
    
    static let UNAUTHORIZED_ERROR_MESSAGE = "Unauthorized access!"
    
    // Sign in/ Sign up
    static let USER_SESSION_EXPIRED_ERROR_MESSAGE = "User session has been expired. Please sign in to your MillionSpaces Account to continue."
    static let SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE = "Invalid Email/Password. Check your credentials and try again!"
    static let SIGN_UP_FAILED_ERROR_MESSAGE = "Your account creation was not successful!. Please try again."
    
    // Home
    static let HOME_LIST_MYSPACE_ALERT_MESSAGE = "Please visit MillionSpaces on the web to list your space.!"
    
    // Space
    static let LOCATION_SORT_WARNING_MESSAGE = "Please select a location filter to sort by distance!"
    
    // Booking
    static let TIME_SELECTION_WARNING_MESSAGE = "Please select a time range for the booking!"
    static let GUEST_CALENDAR_BOOKING_FAILS_ERROR_MESSAGE = "You just missed it! Sorry, your booking time is unavailable."
    static let NOTICE_PERIOD_SELECTION_WARNING_MESSAGE = "Contact the MillionSpaces call centre on \(Constants.MS_PHONE_NUMBER_LABEL) for short notice bookings."
    static let TEST_BOOKING_CHARGE_ERROR = "Booking Charge Validation Error"
    
    // PAYMENT SESSION TIMER
    static let PAYMENT_SESSION_TIME_UP = "Session for the reservation confirmation has expired. Return to Home."
    static let PAYMENT_SESSION_TIME_UP_IPG = "Session for the payment confirmation has expired. Return to Home."
    
    // Payment Summary
    static let PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_1 = "Manual payment options are only applicable for bookings with a 24hr payment window."
    static let PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_2 = "You already have another pending payment."
    
    // PAYMENT
    // IPG
    static let INVALID_AMOUNT_ERROR_MESSAGE = "Invalid amount to process at the moment!"
    static let IPG_FAILURE_ERROR_MESSAGE = "We are experiencing an error loading the payment page. Please reload to try again.!"
    static let CANCEL_IPG_WARNING_MESSAGE = "Are you sure you want to go back and change the payment option?"
    // Tentative Reservation
    static let TENTATIVE_RESERVATION_CONFIRMATION_MESSAGE = "Are you sure you want to confirm the reservation?"
    static let TENTATIVE_RESERVATION_SUCCESS_MESSAGE = "Your tentative reservation for this space was successful. You have 24 hours to manually complete the payment."
    static let TENTATIVE_RESERVATION_EXISTING_PAYMENT_ERROR_MESSAGE = "You already have another pending payment."
    static let TENTATIVE_RESERVATION_FAILURE_MESSAGE = "Your tentative reservation was not successful. Try again."
    
    static let PAYMENT_CONFIRMATION_TIMEOUT_ERROR_MESSAGE = "You session has expired."
    
    // MySpaces
    static let EDIT_MYSPACE_MESSAGE = "Contact us on the MillionSpaces hotline \(Constants.MS_PHONE_NUMBER_LABEL) to make your changes."
    
    static let CONFIRMATION_TO_PROCEED_MESSAGE = "Are you sure you want to proceed?"
    static let TEMP_FEATURE_NOT_IMPLEMENTED_MESSAGE = "Feature is not implemented yet."
    
    // My Profile
    static let EDIT_PROFILE_SUCCESS = "Your profile details were successfully updated."
    static let EDIT_PROFILE_FAILURE = "Your profile detials couldn't be updated. Try again."
    
    // Cancel Booking
    static let CANCEL_BOOKING_CONFIRMATION_MESSAGE = "Are you sure you want to cancel?"
    static let CANCEL_BOOKING_SUCCESS_MESSAGE = "Booking cancellation was successful."
    static let CANCEL_BOOKING_ERROR_MESSAGE = "Booking cancellation was unsuccessful. Try again."
    
    // Review Booking
    static let REVIEW_BOOKING_SUCCESS_MESSAGE = "Review was successfully added."
    static let REVIEW_BOOKING_ERROR_MESSAGE = "Review was not successful. Try Again"
    
    // Manual Booking
    static let MANUAL_BOOKING_NEW_CONFIRMATION_MESSAGE = "Are you sure you want to make this manual booking"
    static let MANUAL_BOOKING_UPDATE_CONFIRMATION_MESSAGE = "Are you sure you want to update this manual booking with the made changes?"
    static let MANUAL_BOOKING_SUCCESS_MESSAGE = "Your manual booking was successfully made."
    static let MANUAL_BOOKING_ERROR_MESSAGE = "This booking cannot be made as it overlaps with an existing booking."
    
    // Promo code
    static let VERIFY_PROMO_CODE_MESSAGE = (One : "Promo code was successfully applied.", Two : "Invalid promo code.", Three : "Entered promo code is expired.")
    
    
    /*
    2-code is invalid
    1-Promo code successfully applied
    3-the code is invalid due to expiry of the promo code
     */
}
