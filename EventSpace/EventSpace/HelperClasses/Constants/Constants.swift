//
//  Constants.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 10/6/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class Constants: NSObject {

    
    // MARK: - UserDefaults
    static let USER_DEFAULTS = UserDefaults.standard
    
    
    // MARK: Notification Identifiers
    static let APPLICATION_IN_FOREGROUND = "ApplicationWillEnterForeground"
    static let GOOGLE_ACCESS_TOKEN_RECEIVED = "GOOGLE_ACCESS_TOKEN_RECEIVED"
    static let APPLICATION_DID_ENTER_BACKGROUND_TIMESTAMP = "APPLICATION_DID_ENTER_BACKGROUND_TIMESTAMP"
    static let PAYMEMT_SESSION_IN_PROGRESS = "PAYMEMT_SESSION_IN_PROGRESS"
    static let PAYMEMT_SESSION_IS_UP = "PAYMEMT_SESSION_IS_UP"
    
    
    // MARK: - Authentication Cookie Handling
    static let KEY_AUTH_COOKIE_STORAGE_DATA = "AUTH_COOKIE_STORAGE_DATA"
    static let KEY_AUTH_RESPONSE_COOKIES = "AUTH_RESPONSE_COOKIES"
    static let KEY_AUTH_RESPONSE_URL = "AUTH_RESPONSE_URL"
    

    // MARK: - Network status codes/error codes
    static let ERROR_CODE_REQUEST_TIMEOUT = -1001
    static let ERROR_CODE_NETWORK_CONNECTION_LOST = -1005
    static let ERROR_CODE_INTERNET_OFFLINE = -1009
    static let STATUS_CODE_REQUEST_SUCCESS = 200
    static let STATUS_CODE_REQUEST_SUCCESS_RANGE = 200...299
    static let STATUS_CODE_UNAUTHORIZED = 403
    static let STATUS_CODE_SERVER_ERROR_RANGE = 500...599
    static let STATUS_CODE_SERVER_GATEWAY_TIMEOUT = 504
    
    
    // MARK: - MillionSpaces Details
    static let MS_PHONE_NUMBER_LABEL = "011 7 811 811"
    static let MS_PHONE_NUMBER = "0117811811"
    
    
    // MARK: - MillionSpaces Bank Account Details
    static let MS_BANK_ACCOUNT_HOLDER_NAME = "MillionSpaces (Pvt) Ltd"
    static let MS_BANK_ACCOUNT_NUMBER = "002010566163"
    static let MS_BANK_NAME = "Hatton National Bank"
    static let MS_BANK_BRANCH_NAME = "City Office"
    static let MS_BANK_ACCOUNT_SWIFT_CODE = "HBLILKLX"
    
    
    // MARK: - Common
    static let MS_TERMS_PRIVACY_POLICY = "https://beta.millionspaces.com/#/terms/privacy"
    
}
