//
//  FilterDropDownView.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/3/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class FilterDropDownView: UIView {
    
    @IBOutlet weak var filterTableView: UITableView!
    
    var filterData: NSArray!
    var filterType: String!
    
    override func awakeFromNib() {
        self.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    /*
    func initView(filterData: NSArray, filterType: String, frameX: Double, frameY: Double, frameWidth: Double, frameHeight: Double){
        self.frame = CGRect(x: frameX, y: frameY, width: frameWidth, height: frameHeight)
        self.filterData = filterData
        self.filterType = filterType
        
        self.filterTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }*/
    
    /*
    // Only override draw() if you perform custom drawing.
    // An 
     empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
}

extension FilterDropDownView: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        self.configureCell(cell: cell, at: indexPath)
        return cell
    }
    
    func configureCell(cell: UITableViewCell, at indexPath: IndexPath) {
        
        if self.filterType == "EventType" {
            let eventType = self.filterData[indexPath.row] as! EventType
            cell.textLabel?.text = eventType.value(forKey: "name") as? String
        }
        else {
            cell.textLabel?.text = self.filterData[indexPath.row] as? String
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

