//
//  DropDownView.swift
//  EventSpace
//
//  Created by Randika Swaris on 5/15/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class DropDownView: UIView {
    
    @IBOutlet weak var view: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        Bundle.main.loadNibNamed("DropDownView", owner: self, options: nil)
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(self.view)
    }
}
