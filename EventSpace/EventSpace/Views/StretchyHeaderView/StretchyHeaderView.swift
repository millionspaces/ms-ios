//
//  StretchyHeaderView.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/20/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class StretchyHeaderView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage? {
        didSet {
            if let image = image {
                imageView.image = image
            }
            else{
                imageView.image = nil
            }
        }
        
        
    }

}
