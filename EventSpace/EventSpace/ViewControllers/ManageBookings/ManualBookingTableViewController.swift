//
//  ManualBookingTableViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 11/21/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class ManualBookingTableViewController: BaseTableViewController, UITextFieldDelegate, UITextViewDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet var screenTableView: UITableView!
    @IBOutlet weak var parentContainerView: UIView!
    
    //Date Time Section
    @IBOutlet weak var bookingStartDateTImeLabel: UILabel!
    @IBOutlet weak var bookingEndDateTImeLabel: UILabel!
    
    // Guest Details Section
    @IBOutlet weak var guestNameText: UITextField!
    @IBOutlet weak var guestContactNoText: UITextField!
    @IBOutlet weak var guestEmailText: UITextField!
    @IBOutlet weak var spaceReservationFeeText: UITextField!
    
    // CVs View
    @IBOutlet weak var constCvParentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constEventTypeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constSeatingViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var eventTypeCollectionView: UICollectionView!
    @IBOutlet weak var seatingCollectionView: UICollectionView!
    
    // Other View
    @IBOutlet weak var expectedGuestCountText: UITextField!
    @IBOutlet weak var bookingStatusLabel: UILabel!
    @IBOutlet weak var amenitiesTextView: UITextView!
    @IBOutlet weak var remarksTextView: UITextView!
    
    // Date Time Picker Views
    @IBOutlet weak var viewForDateTimePickers: UIView!
    @IBOutlet weak var pickersViewCancelButton: UIButton!
    @IBOutlet weak var pickersViewSetButton: UIButton!
    @IBOutlet weak var constViewForDateTimePickersSubviewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var hourPicker: UIPickerView!
    @IBOutlet weak var amPmPicker: UIPickerView!

    
    // MARK: - Class Variables
    var bookingSpace: Space!
    
    var manualBookingEventType: EventType?
    var manualBookingSeatingArrangement: SpaceSeatingArrangement?
    
    
    
    var eventTypesCVHeight: CGFloat = 100.0
    var seatingCVHeight: CGFloat = 100.0
    var ecvCellWidth: CGFloat!
    let ecvCellHeight: CGFloat = 100.0
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func configureView() {
        self.navigationItem.title = "Manual Booking"
        
        self.addKeyBoardDismissOnTap()
        
        eventTypesCVHeight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.eventTypes.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        seatingCVHeight = Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.seatingArrangements.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        
        ecvCellWidth = (Utils.screenWidth - 20.0) / 3.0
        
        
        self.constEventTypeViewHeight.constant = eventTypesCVHeight + 50.0
        self.constSeatingViewHeight.constant = seatingCVHeight + 50.0
        self.constCvParentViewHeight.constant = eventTypesCVHeight + seatingCVHeight + 110.0
        
        let parentContentViewHeightToBe = 1560.0 - 310.0 + (eventTypesCVHeight + seatingCVHeight + 110.0)
        self.parentContainerView.frame = CGRect.init(x: 0.0, y: 0.0, width: Utils.screenWidth, height: parentContentViewHeightToBe)
        
        /// 50 + content height for one cv
        // 310, 150, 150
    
        // 1560
    }

    
    // MARK: IBActions
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }

    // Date Time Section
    @IBAction func bookingStartDateTimeSelectionButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func bookingEndDateTimeSelectionButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func bookingStatusSelectionButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


// MARK: - UICollectionViewDataSource and Delegate
extension ManualBookingTableViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.eventTypeCollectionView {
            return bookingSpace.eventTypes.count
        }
        else if collectionView == self.seatingCollectionView  {
            return bookingSpace.seatingArrangements.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collectionView didSelectItemAt")
        
        if collectionView == self.eventTypeCollectionView {
            self.manualBookingEventType = bookingSpace.eventTypes[indexPath.item] as? EventType
            let selectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
        else if collectionView == self.seatingCollectionView  {
            self.manualBookingSeatingArrangement = (bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement)
            let selectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("collectionView didDeselectItemAt")
        
        if collectionView == self.eventTypeCollectionView {
            let deselectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
            
        }
        else if collectionView == self.seatingCollectionView  {
            let deselectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectableItemCell", for: indexPath)
        
        let backgroundView = collectionViewCell.viewWithTag(1)
        
        let itemLabel = collectionViewCell.viewWithTag(2) as! UILabel
        let itemIconImageView = collectionViewCell.viewWithTag(3) as! UIImageView
        
         if collectionView == self.eventTypeCollectionView  {
            let eventTypeAtIndex = bookingSpace.eventTypes[indexPath.item] as! EventType
            itemLabel.text = eventTypeAtIndex.name!
            itemIconImageView.sd_setImage(with: URL(string: eventTypeAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if manualBookingEventType == eventTypeAtIndex {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        else if collectionView == self.seatingCollectionView {
            let seatingAtIndex = bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement
            itemLabel.text = seatingAtIndex.arrangement.name!
            itemIconImageView.sd_setImage(with: URL(string: seatingAtIndex.arrangement.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if let selectedSpaceSeatingA = manualBookingSeatingArrangement?.arrangement, selectedSpaceSeatingA.id == Int16(seatingAtIndex.id) {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        return collectionViewCell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ManualBookingTableViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ecvCellWidth, height: ecvCellHeight)
    }
}
