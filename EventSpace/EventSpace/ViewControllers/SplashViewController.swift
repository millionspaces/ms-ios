//
//  SplashViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 8/14/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var constBestPricesImgBottom: NSLayoutConstraint!
    
    @IBOutlet weak var bestPricesImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        print("SplashViewController viewDidLoad")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        UIApplication.shared.isStatusBarHidden = true
        super.viewWillAppear(animated)

        self.bestPricesImageView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        MetaData.deleteAll()
        
        self.bestPricesImageView.transform = CGAffineTransform.init(scaleX: 0.01, y: 0.01)
        UIView.transition(with: self.bestPricesImageView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.bestPricesImageView.transform = CGAffineTransform.identity
            self.bestPricesImageView.isHidden = false
        }, completion: nil)
        
        self.delay(1.0) {
              self.executeStartUpFunctions()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        UIApplication.shared.isStatusBarHidden = false
        super.viewWillDisappear(animated)
    }
    
    func callHomeSegue() {
        self.performSegue(withIdentifier: "HomeSegue", sender: self)
    }
    
    func executeStartUpFunctions() {
        self.activityIndicator.startAnimating()
    
        MetaData.saveAllMetaData { (completion: Bool, statusCode: Int) in
            if completion {
                if AuthenticationManager.isUserAuthenticated {
                    self.retrieveAuthenticatedUserDetails()
                }
                else {
                    DispatchQueue.main.sync(execute: {
                        self.activityIndicator.stopAnimating()
                        self.callHomeSegue()
                    })
                }
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.activityIndicator.stopAnimating()
                    if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(statusCode) {
                        self.addReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                    }
                    else if statusCode == Constants.ERROR_CODE_INTERNET_OFFLINE || statusCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                        self.addReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                    }
                    else {
                        self.addReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    }
                })
            }
        }
    }
    
    
    // MARK: - Retrieve user details for authenticated user
    func retrieveAuthenticatedUserDetails() {
        WebServiceCall.retrieveUser(successBlock: { (results: Any) in
            print("SplashVC userDetails | results : \(results)")
            DispatchQueue.main.sync(execute: {
                AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                self.activityIndicator.stopAnimating()
                self.callHomeSegue()
            })
        })
        { (errorCode: Int, error: String) in
            print("SplashVC userDetails | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.activityIndicator.stopAnimating()
                
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    AuthenticationManager.logOutAuthenticatedUser()
                    self.callHomeSegue()
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func addReloadAlert(message: String) {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.executeStartUpFunctions()
        }
        alertController.addAction(reloadAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
