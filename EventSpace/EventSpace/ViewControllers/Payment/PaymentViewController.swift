//
//  PaymentViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/5/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import WebKit

class PaymentViewController: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var cardOptionButton: UIButton!
    @IBOutlet weak var onlineTransferOptionButton: UIButton!
    @IBOutlet weak var bankDepositOptionButton: UIButton!
    
    
    @IBOutlet weak var manualOptionOneLabel: UILabel!
    @IBOutlet weak var manualOptionOneWarningLabel: UILabel!
    @IBOutlet weak var manualOptionTwoLabel: UILabel!
    @IBOutlet weak var manualOptionTwoWarningLabel: UILabel!

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var timerBarButton: UIBarButtonItem!
    
    var alertController: UIAlertController?
    
    // MARK: - Class constants/variables
    var bookingSpace: Space!
    var bookingRequest: SpaceBookingRequest!
    var bookingResponse: [String : Any]!
    
    var referenceIdStr: String!
    var spaceNameStr: String!
    var locationStr: String!
    var dateStr: String!
    var reservationTimeStr: String!
    var subTotalStr: String!
    var selectedOption: String! // SegueIDs are assigned
    
    var areManualPaymentOptionsDisabled = false
    var manualPaymentDisabledAlertMessage: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaymentViewController viewDidLoad")
        self.view.layoutIfNeeded()
    
        PaymentSessionTimer.sharedTimer.startPaymentSessionTimer()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addPaymentSessionTimerLabelToNavBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerInProgress(_:)), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IN_PROGRESS), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePaymentSessionTimerExpiry), name: NSNotification.Name(rawValue: Constants.PAYMEMT_SESSION_IS_UP), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.popToHomeWhenAppGoesForeground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        /*if let timerIsValid = PaymentSessionTimer.sharedTimer.internalTimer?.isValid, !timerIsValid {
            PaymentSessionTimer.sharedTimer.resumePaymentSessionTimer()
        }*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.removePaymentSessionTimerLabelFromNavBar()
        //PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        NotificationCenter.default.removeObserver(self)
    }
    
    func popToHomeWhenAppGoesForeground() {
        alertController?.dismiss(animated: false, completion: nil)
        self.popToRootViewController()
        // TODO: Invalidate Payment Timer
    }
    
    func configureView() {
        
        self.navigationItem.title = "Payment Summary"
        
        // Default selected Payment Option
        self.cardOptionButton.isSelected = true
        selectedOption = "IpgSegue"
        self.manualOptionOneWarningLabel.isHidden = true
        self.manualOptionTwoWarningLabel.isHidden = true
        
        // Show Summary
        self.referenceIdStr = (bookingResponse["orderId"] as! String)
        
        self.spaceNameStr = bookingSpace.name!
        
        self.locationStr = bookingSpace.fullAdrress!
  
        self.dateStr = bookingRequest.bookingDate.simpleDate_2!
        
        self.reservationTimeStr = ""
        if bookingRequest.bookingTimes.count == 1 {
            self.reservationTimeStr = bookingRequest.bookingTimes[0]
        }
        else {
            for t in bookingRequest.bookingTimes {
                self.reservationTimeStr.append("\(t) \n")
            }
            self.reservationTimeStr = String(self.reservationTimeStr.characters.dropLast(1))
        }
        
        self.subTotalStr = "LKR \(Utils.formatPriceValues(price: bookingRequest.bookingCharge!))"
        
        self.validatedManualPaymentOptionsAvailability()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 50.0
        self.tableView.reloadData()
    }
    
    
    func validatedManualPaymentOptionsAvailability() {
        if !(self.bookingResponse["isUserEligibleForManualPayment"] as! Bool) {
            //print("isUserEligibleForManualPayment == 0")
            self.areManualPaymentOptionsDisabled = true
            self.manualPaymentDisabledAlertMessage = AlertMessages.PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_2
        }
        else if !(self.bookingResponse["isBookingTimeEligibleForManualPayment"] as! Bool) {
            //print("isBookingTimeEligibleForManualPayment == 0")
            self.areManualPaymentOptionsDisabled = true
            self.manualPaymentDisabledAlertMessage = AlertMessages.PAYMENT_OPTIONS_MANUAL_OPTIONS_DISABLED_1
        }
    }
    
    func handlePaymentSessionTimerInProgress(_ notification: Notification) {
        if let timerValue = notification.userInfo?["timer"] {
            print("PaymentVC handlePaymentSessionTimerInProgress | timerValue : \(timerValue)")
            self.setPaymentSessionTimerLabelText(text: timerValue as! String)
        }
    }

    func handlePaymentSessionTimerExpiry() {
        alertController = UIAlertController(title: AlertMessages.ALERT_TITLE, message: AlertMessages.PAYMENT_SESSION_TIME_UP, preferredStyle: .alert)
        let returnHomeAction = UIAlertAction(title: "Go to Home", style: .default) { (alert: UIAlertAction) in
            self.popToRootViewController()
        }
        alertController!.addAction(returnHomeAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        PaymentSessionTimer.sharedTimer.stopPaymentSessionTimer()
        self.popViewController()
    }
    
    
    @IBAction func paymentOptionPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            print("PaymentVC paymentOptionPressed - 1")
            selectedOption = "IpgSegue"
            self.cardOptionButton.isSelected = true
            self.onlineTransferOptionButton.isSelected = false
            self.bankDepositOptionButton.isSelected = false
            self.manualOptionOneWarningLabel.isHidden = true
            self.manualOptionTwoWarningLabel.isHidden = true
        }
        else if sender.tag == 2 {
            if areManualPaymentOptionsDisabled {
                self.presentDismissableAlertWithMessage(title: AlertMessages.MANUAL_PAYMENT_OPTIONS_DISABLED_TITLE, message: self.manualPaymentDisabledAlertMessage!)
            }
            else {
                print("PaymentVC paymentOptionPressed - 2")
                selectedOption = "ManualPaymentInfoSegue"
                self.cardOptionButton.isSelected = false
                self.onlineTransferOptionButton.isSelected = true
                self.bankDepositOptionButton.isSelected = false
                self.manualOptionOneWarningLabel.isHidden = false
                self.manualOptionTwoWarningLabel.isHidden = true
            }
        }
        else if sender.tag == 3 {
            if areManualPaymentOptionsDisabled {
                self.presentDismissableAlertWithMessage(title: AlertMessages.MANUAL_PAYMENT_OPTIONS_DISABLED_TITLE, message: self.manualPaymentDisabledAlertMessage!)
            }
            else {
                print("PaymentVC paymentOptionPressed - 3")
                selectedOption = "ManualPaymentInfoSegue"
                self.cardOptionButton.isSelected = false
                self.onlineTransferOptionButton.isSelected = false
                self.bankDepositOptionButton.isSelected = true
                self.manualOptionOneWarningLabel.isHidden = true
                self.manualOptionTwoWarningLabel.isHidden = false
            }
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        print("PaymentVC nextButtonPressed")
        performSegue(segueID: selectedOption)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "IpgSegue" {
            if let ipgvc = segue.destination as? IPaymentGatewayViewController {
                print("bookingResponse : \(bookingResponse)")
                ipgvc.orderId = bookingResponse["id"] as! Int
                ipgvc.bookingId = bookingResponse["orderId"] as! String //(bookingResponse["id"] as! Int)
                ipgvc.chargeAmount = bookingRequest.bookingCharge!
            }
        }
        else if segue.identifier == "ManualPaymentInfoSegue" {
            if let manualVC = segue.destination as? ManualPaymentInfoViewController {
                manualVC.chargeAmountStr = self.subTotalStr!
                manualVC.orderId = bookingResponse["id"] as! Int
                manualVC.bookingId = self.referenceIdStr!
            }
        }
    }
}


extension PaymentViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.viewWithTag(0)?.backgroundColor = (indexPath.row % 2 == 0) ? UIColor.groupTableViewBackground : UIColor.white
        
        let label = cell.viewWithTag(1) as! UILabel
        let valueLabel = cell.viewWithTag(2) as! UILabel
        
        if indexPath.row == 0 {
            label.text = "Reference ID"
            valueLabel.text = self.referenceIdStr
        }
        else if indexPath.row == 1 {
            label.text = "Space"
            valueLabel.text = self.spaceNameStr
        }
        else if indexPath.row == 2 {
            label.text = "Location"
            valueLabel.text = self.locationStr
        }
        else if indexPath.row == 3 {
            label.text = "Date"
            valueLabel.text = self.dateStr
        }
        else if indexPath.row == 4 {
            label.text = "Reservation Time"
            valueLabel.text = self.reservationTimeStr
        }
        else if indexPath.row == 5 {
            label.text = "Sub Total"
            valueLabel.text = self.subTotalStr
            label.font = UIFont.init(name: "HelveticaNeue-Bold", size: 16.0)
            valueLabel.font = UIFont.init(name: "HelveticaNeue-Medium", size: 16.0)
        }
        
        return cell
    }
}
