//
//  BaseViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 6/14/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    private var backgroundViewForActivityIndicator: UIView?
    private var baseActivityIndicator: UIActivityIndicatorView?
    
    var baseCommonAlertController: UIAlertController?
    
    var paymentSessionTimerLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initBaseActivityIndicator()
    }
    
    // MARK: - Initialize Base Activity Indicator
    private func initBaseActivityIndicator() {
        // Background UIView
        self.backgroundViewForActivityIndicator = UIView.init(frame: self.view.bounds)
        self.backgroundViewForActivityIndicator?.backgroundColor = UIColor.clear //UIColor.black
        //self.backgroundViewForActivityIndicator?.alpha = 0.4
        
        // UIActivityIndicatorView
        self.baseActivityIndicator = UIActivityIndicatorView()
        self.baseActivityIndicator!.frame = CGRect(x: self.view.frame.size.width/2 - 25.0, y: self.view.frame.size.height/2 - 25.0, width: 50.0, height: 50.0)
        self.baseActivityIndicator!.activityIndicatorViewStyle = .whiteLarge
        self.baseActivityIndicator!.color = UIColor.init(red: 0.0, green: 142.0/255.0, blue: 218.0/255.0, alpha: 1.0)
        self.baseActivityIndicator!.hidesWhenStopped = true
        
        self.backgroundViewForActivityIndicator?.addSubview(self.baseActivityIndicator!)
    }
    
    func showActivityIndicator() {
        self.baseActivityIndicator?.startAnimating()
        self.view.addSubview(self.backgroundViewForActivityIndicator!)
    }
    
    func hideActivityIndicator() {
        self.backgroundViewForActivityIndicator?.removeFromSuperview()
        self.baseActivityIndicator?.stopAnimating()
    }
    
    // MARK: delay functions
    func delay(_ delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    // MARK: - Set Window Orientation Restriction
    func setWindowOrientationRestriction(restriction: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.deviceOrientationRestriction = restriction ? "Portrait" : "All"
    }
    
    // MARK: Keyboard Dismiss on tap
    func addKeyBoardDismissOnTap() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Open View Controllers Modally
    func openSignInViewController() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInViewController
        let navController = UINavigationController.init(rootViewController: signInVC)
        navController.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.present(navController, animated: true, completion: nil)
    }
    
    func openNotificationViewController() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationNC")
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    func performSegue(segueID: String) {
        self.performSegue(withIdentifier: segueID, sender: self)
    }
    
    
    func dismissViewController() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(viewController: Swift.AnyClass) {
        self.navigationController?.backToViewController(viewController: viewController.self)
    }
    
    func popToSplashViewController() {
        self.popToViewController(viewController: SplashViewController.classForCoder())
    }

    // MARK: - add done button for number pad style keyboard
    func addNumberPadDoneButton(textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.tintColor = UIColor.white
        keyboardToolbar.barTintColor = UIColor.init(colorLiteralRed: 63.0/255.0, green: 63.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    // MARK: - UIAlertControllers
    func presentServerErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func presentNetworkUnavailabilityErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func presentDismissableAlertWithMessage(title: String, message: String) {
        self.baseCommonAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        self.baseCommonAlertController!.addAction(dismissAction)
        self.present(self.baseCommonAlertController!, animated: true, completion: nil)
    }
    
    // MARK: Payment Session Timer Label
    func addPaymentSessionTimerLabelToNavBar() {
        if let navigationBar = self.navigationController?.navigationBar {
            self.paymentSessionTimerLabel = UILabel.init(frame: CGRect(x: Utils.screenWidth - 60.0, y: 10.0, width: 60.0, height: 24.0))
            self.paymentSessionTimerLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 18.0)
            self.paymentSessionTimerLabel?.textColor = .white
            self.paymentSessionTimerLabel?.layer.masksToBounds = true
            //self.paymentSessionTimerLabel?.backgroundColor = .black
            //self.paymentSessionTimerLabel?.text = "--:--"
            navigationBar.addSubview(self.paymentSessionTimerLabel!)
        }
    }
    
    func setPaymentSessionTimerLabelText(text: String) {
        self.paymentSessionTimerLabel?.text = text
    }
    
    func removePaymentSessionTimerLabelFromNavBar() {
        self.paymentSessionTimerLabel?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UINavigationController {
    
    func backToViewController(viewController: Swift.AnyClass) {
        for element in viewControllers {
            if element.isKind(of: viewController) {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
