//
//  BaseTableViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 11/23/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {

    private var backgroundViewForActivityIndicator: UIView?
    private var baseActivityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initBaseActivityIndicator()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    // MARK: - Initialize Base Activity Indicator
    private func initBaseActivityIndicator() {
        // Background UIView
        self.backgroundViewForActivityIndicator = UIView.init(frame: self.view.bounds)
        self.backgroundViewForActivityIndicator?.backgroundColor = UIColor.clear //UIColor.black
        //self.backgroundViewForActivityIndicator?.alpha = 0.4
        
        // UIActivityIndicatorView
        self.baseActivityIndicator = UIActivityIndicatorView()
        self.baseActivityIndicator!.frame = CGRect(x: self.view.frame.size.width/2 - 25.0, y: self.view.frame.size.height/2 - 25.0, width: 50.0, height: 50.0)
        self.baseActivityIndicator!.activityIndicatorViewStyle = .whiteLarge
        self.baseActivityIndicator!.color = UIColor.init(red: 0.0, green: 142.0/255.0, blue: 218.0/255.0, alpha: 1.0)
        self.baseActivityIndicator!.hidesWhenStopped = true
        
        self.backgroundViewForActivityIndicator?.addSubview(self.baseActivityIndicator!)
    }
    
    func showActivityIndicator() {
        self.baseActivityIndicator?.startAnimating()
        self.view.addSubview(self.backgroundViewForActivityIndicator!)
    }
    
    func hideActivityIndicator() {
        self.backgroundViewForActivityIndicator?.removeFromSuperview()
        self.baseActivityIndicator?.stopAnimating()
    }
    
    // MARK: delay functions
    func delay(_ delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    // MARK: - Set Window Orientation Restriction
    func setWindowOrientationRestriction(restriction: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.deviceOrientationRestriction = restriction ? "Portrait" : "All"
    }
    
    // MARK: Keyboard Dismiss on tap
    func addKeyBoardDismissOnTap() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Open View Controllers Modally
    func openSignInViewController() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInVC") as! SignInViewController
        let navController = UINavigationController.init(rootViewController: signInVC)
        navController.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.present(navController, animated: true, completion: nil)
    }
    
    func openNotificationViewController() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NotificationNC")
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    func performSegue(segueID: String) {
        self.performSegue(withIdentifier: segueID, sender: self)
    }
    
    
    func dismissViewController() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func popToViewController(viewController: Swift.AnyClass) {
        print("BaseVC popToViewController")
        self.navigationController?.backToViewController(viewController: viewController.self)
    }
    
    // MARK: - add done button for number pad style keyboard
    func addNumberPadDoneButton(textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.tintColor = UIColor.white
        keyboardToolbar.barTintColor = UIColor.init(colorLiteralRed: 63.0/255.0, green: 63.0/255.0, blue: 63.0/255.0, alpha: 1.0)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
    
    // MARK: - UIAlertControllers
    func presentServerErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SERVER_ERROR_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentNetworkUnavailabilityErrorAlert() {
        let alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: AlertMessages.INTERNET_OFFLINE_MESSAGE_2, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // TEMP
    func presentDismissableAlertWithMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .cancel, handler: nil)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
