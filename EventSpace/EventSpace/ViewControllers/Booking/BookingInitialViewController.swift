//
//  BookingInitialViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 9/11/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class BookingInitialViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var BookingInitialTableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!
    
    var bookingSpace: Space!
    
    var eventTypesECVHeight: CGFloat = 0.0
    var seatingECVHeight: CGFloat = 0.0
    var ecvCellWidth: CGFloat!
    let ecvCellHeight: CGFloat = 100.0
    
    var selectedEventType: EventType?
    var selectedSpaceSeatingArrangement: SpaceSeatingArrangement?
    var selectedGuestCount: Int?

    var currentMaximumParticipantCount: Int!
    
    var hasValidDetails = false
    
    let initialPlaceholderColor = UIColor.init(red: 199.0/255.0, green: 199.0/255.0, blue: 205.0/255.0, alpha: 1.0)
    let invalidPlaceholderColor = UIColor.red
    let textFieldValidFontColor = UIColor.black
    let textFieldInvalidFontColor = UIColor.red

    let maxAlertMessage = "Your expected guest count exceeds the maximum capacity. The host may or may not fulfill your request."
    let minAlertMessage = "Your expected guest count is below the minimum capacity. You will be charged for the minimum defined by the host."
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToBookingInitialScreenFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func returnedToBookingInitialScreenFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) { // ~ 10s
            self.popToRootViewController()
        }
    }
    
    func configureView() {
        self.navigationItem.title = "Booking Engine"
        print("BookingInitialVC configureView | Booking Space : \(bookingSpace.id!, bookingSpace.name!, bookingSpace.availabilityMethod!)")
        print("BookingInitialVC configureView | Booking Space Event Types Count: \(bookingSpace.eventTypes.count)")
        print("BookingInitialVC configureView | Booking Space Seating Arr Count: \(bookingSpace.seatingArrangements.count)")
        
        addKeyBoardDismissOnTap()
        
        eventTypesECVHeight = 60 + Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.eventTypes.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        seatingECVHeight = 60 + Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: self.bookingSpace.seatingArrangements.count, cvCellHeight: Int(ecvCellHeight), itemsPerRow: 3)
        
        ecvCellWidth = (Utils.screenWidth - 20.0) / 3.0
        
        self.nextButton.backgroundColor = UIColor.lightGray
        
        updateCurrentMaximumParticipantCount()
    }
    
    func updateCurrentMaximumParticipantCount() {
        if let selectedSpaceSeatingA = self.selectedSpaceSeatingArrangement {
            self.currentMaximumParticipantCount = selectedSpaceSeatingA.participantCount!
        }
        else {
            self.currentMaximumParticipantCount = bookingSpace!.participantCount!
        }
        
        self.BookingInitialTableView.reloadData()
    }
    
    func prepareExpectedGuestCountPlaceholderText() -> String {
        if bookingSpace!.blockChargeType == 2 {
            return "Min. - \(bookingSpace!.minimumParticipantCount)  |  Max. - \(self.currentMaximumParticipantCount!)"
        }
        else {
            return "Max. - \(self.currentMaximumParticipantCount!)"
        }
    }
    
    func prepareLimitWarningMessage(forExpectedGuestCount count: Int) -> String {
        if bookingSpace!.blockChargeType == 2 {
            if count < self.bookingSpace!.minimumParticipantCount {
                return self.minAlertMessage
            }
        }
        
        if count > self.currentMaximumParticipantCount {
            return self.maxAlertMessage
        }
        else {
            return ""
        }
    }

    func openBookingCalendarScreen() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let bookingCalendarVC = storyboard.instantiateViewController(withIdentifier: "BookingCalendarVC") as! BookingCalendarViewController
        bookingCalendarVC.bookingSpace = bookingSpace!
        bookingCalendarVC.selectedEventType = selectedEventType!
        bookingCalendarVC.selectedSeatingArrangement = selectedSpaceSeatingArrangement!.arrangement
        bookingCalendarVC.selectedGuestCount = selectedGuestCount!
        self.navigationController?.pushViewController(bookingCalendarVC, animated: true)
    }

    
    // MARK: - IBActions
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        //print("BookingInitialVC NextButtonPressed")
        if hasValidDetails {
            openBookingCalendarScreen()
        }
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openNotificationViewController()
    }
    
    // MARK: - UITextFieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("BookingInitialVC textFieldDidBeginEditing")
        textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: initialPlaceholderColor)
        //if textField.text!.characters.count <= 6 {
            //textField.textColor = textFieldValidFontColor
        //}
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("BookingInitialVC textFieldDidEndEditing")
        if (textField.text?.characters.count)! > 0, let numberFromText = Int(textField.text!) {
            if numberFromText != 0 {
                textField.text = String(numberFromText)
                self.selectedGuestCount = (textField.text!.characters.count <= 6) ? numberFromText : nil
                textField.textColor = (textField.text!.characters.count <= 6) ? textFieldValidFontColor : textFieldInvalidFontColor
            }
            else {
                self.selectedGuestCount = nil
                textField.text = nil
                textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: invalidPlaceholderColor)
            }
        }
        else {
            self.selectedGuestCount = nil
            textField.text = nil
            textField.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: invalidPlaceholderColor)
        }
        
        validateSelection()
        self.BookingInitialTableView.reloadData()
    }
    
    func textChanged(_ textField: UITextField) {
        if textField.text!.characters.count > 6 {
            //textField.textColor = textFieldInvalidFontColor
            textField.deleteBackward()
        }
        else {
            textField.textColor = textFieldValidFontColor
        }
    }
    
    
    func validateSelection() {
        hasValidDetails = false
        self.nextButton.backgroundColor = UIColor.lightGray
        
        guard selectedEventType != nil else {
            return
        }
        
        guard selectedSpaceSeatingArrangement != nil else {
            return
        }
        
        guard selectedGuestCount != nil else {
            let guestCountCell = self.BookingInitialTableView.cellForRow(at: IndexPath.init(row: 0, section: 0))
            if let guestCountText = guestCountCell?.viewWithTag(1) as? UITextField {
                guestCountText.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: invalidPlaceholderColor)
            }
            return
        }
        
        hasValidDetails = true
        self.nextButton.backgroundColor = ColorUtils.MS_BLUE_COLOR
        
        //openBookingCalendarScreen()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


//CollectionViewEmbeddedCell label - 1, cv - 0
//SelectableItemCell view - 1, label -2, image - 3

extension BookingInitialViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        else if indexPath.row == 1 {
            return eventTypesECVHeight
        }
        else {
            return seatingECVHeight
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        else if indexPath.row == 1 {
            return eventTypesECVHeight
        }
        else {
            return seatingECVHeight
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let guestCountCell = tableView.dequeueReusableCell(withIdentifier: "GuestCountCell", for: indexPath)
            if let guestCountText = guestCountCell.viewWithTag(1) as? UITextField {
                guestCountText.setTextFieldPlaceholder(withPlaceholderText: self.prepareExpectedGuestCountPlaceholderText(), andPlaceholderTextColor: .lightGray)
                self.addNumberPadDoneButton(textField: guestCountText)
                guestCountText.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
                
                let alertLabel = guestCountCell.viewWithTag(2) as! UILabel
                
                if guestCountText.text!.characters.count > 0, let numberFromText = Int(guestCountText.text!) {
                    alertLabel.text = self.prepareLimitWarningMessage(forExpectedGuestCount: numberFromText)
                    alertLabel.backgroundColor = ColorUtils.BOOKING_RESERVATION_STATUS_COLORS.CANCELLED
                }
            }
            return guestCountCell
        }
        else {
            let collectionViewEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            collectionViewEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.row)
            collectionViewEmbeddedCell.collectionView.allowsMultipleSelection = false 
            if indexPath.row == 1 {
                (collectionViewEmbeddedCell.viewWithTag(1) as! UILabel).text = "Select Event Type"
            }
            else {
                (collectionViewEmbeddedCell.viewWithTag(1) as! UILabel).text = "Select Seating Arrangement"
            }
            
            return collectionViewEmbeddedCell
        }
    }
}

// MARK: - UICollectionViewDataSource and Delegate
extension BookingInitialViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            return bookingSpace.eventTypes.count
        }
        else {
            return bookingSpace.seatingArrangements.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            self.selectedEventType = bookingSpace.eventTypes[indexPath.item] as? EventType
            let selectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
        }
        else {
            self.selectedSpaceSeatingArrangement = (bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement)
            let selectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = selectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
            updateCurrentMaximumParticipantCount()
        }
        
        validateSelection()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            let deselectedEventTypeCell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedEventTypeCell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
           
        }
        else {
            let deselectedSeatingACell = collectionView.cellForItem(at: indexPath)!
            let backgroundView = deselectedSeatingACell.viewWithTag(1)
            backgroundView?.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectableItemCell", for: indexPath)

        let backgroundView = collectionViewCell.viewWithTag(1)
        
        let itemLabel = collectionViewCell.viewWithTag(2) as! UILabel
        let itemIconImageView = collectionViewCell.viewWithTag(3) as! UIImageView
        
        if collectionView.tag == 1 {
            let eventTypeAtIndex = bookingSpace.eventTypes[indexPath.item] as! EventType
            itemLabel.text = eventTypeAtIndex.name!
            itemIconImageView.sd_setImage(with: URL(string: eventTypeAtIndex.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if selectedEventType == eventTypeAtIndex {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        else {
            let seatingAtIndex = bookingSpace.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement
            itemLabel.text = seatingAtIndex.arrangement.name!
            itemIconImageView.sd_setImage(with: URL(string: seatingAtIndex.arrangement.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if let selectedSpaceSeatingA = selectedSpaceSeatingArrangement?.arrangement, selectedSpaceSeatingA.id == Int16(seatingAtIndex.id) {
                backgroundView?.layer.borderColor = ColorUtils.MS_BLUE_COLOR.cgColor
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
            }
            else {
                backgroundView?.layer.borderColor = UIColor.clear.cgColor
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
        
        return collectionViewCell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BookingInitialViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ecvCellWidth, height: ecvCellHeight)
    }
}
