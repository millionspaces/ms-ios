//
//  UserProfileViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 8/14/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class UserProfileViewController: BaseViewController, UITextFieldDelegate {

    
    @IBOutlet weak var profileTableView: UITableView!

    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var jobTitleTextField: UITextField!
    @IBOutlet weak var companyPhoneTextField: UITextField!
    
    
    var isPageEdited: Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        configureView()
    }
    
    func configureView() {
        self.navigationItem.title = "User Profile"
        
        if let profileImageUrl = AuthenticationManager.user?.profileImageUrl  {
            self.profileImageView.sd_setImage(with: profileImageUrl, placeholderImage: #imageLiteral(resourceName: "ico-camera"))
        }
    
        self.nameTextField.text = AuthenticationManager.user!.name
        self.emailTextField.text = AuthenticationManager.user!.email
        
        if let mobile = AuthenticationManager.user?.mobileNumber {
            self.mobileNumberTextField.text = mobile
        }
        
        if let compName = AuthenticationManager.user?.companyName {
            self.companyNameTextField.text = compName
        }
        
        if let address = AuthenticationManager.user?.companyAddress {
            self.addressTextField.text = address
        }
        
        if let job = AuthenticationManager.user?.jobTitle {
            self.jobTitleTextField.text = job
        }
        
        if let compPhone = AuthenticationManager.user?.companyPhone {
            self.companyPhoneTextField.text = compPhone
        }
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    
    //MARK: - @IBAction
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        popViewController()
    }
    
    @IBAction func updateProfileImageButtonPressed(_ sender: UIButton) {
        print("UserProfileVC updateProfileImageButtonPressed")
        
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        print("UserProfileVC saveButtonPressed")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
