//
//  CollectionViewEmbeddedTableViewCell.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 8/4/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class CollectionViewEmbeddedTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    func setCollectionViewDatasourceDelegate <D: UITableViewDataSource & UITableViewDelegate> (dataSourceDelegate: D, identifier id: Int) {
        self.collectionView.dataSource = dataSourceDelegate as? UICollectionViewDataSource
        self.collectionView.delegate = dataSourceDelegate as? UICollectionViewDelegate
        self.collectionView.tag = id
        self.collectionView.allowsMultipleSelection = true
        self.collectionView.reloadData()
        if self.pageController != nil {
            self.pageController.tag = id
            self.pageController.numberOfPages = (dataSourceDelegate as! UICollectionViewDataSource).collectionView(self.collectionView, numberOfItemsInSection: 0)
        }
    }
}
