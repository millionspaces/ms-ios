//
//  SpaceDetailsViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/26/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import GoogleMaps

struct SpaceDataStructureForTableViewSection {
    var section: Int!
    var sectionSpecialIdentifier: Int!
    var numOfRows: Int!
}

class SpaceDetailsViewController: BaseViewController {
    
    @IBOutlet weak var spaceDetailsTableView: UITableView!
    @IBOutlet weak var bookNowButton: UIButton!
    @IBOutlet weak var constBookNowViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var fullScreenImageSliderView: UIView!
    @IBOutlet weak var fullScreenImageCollectionView: UICollectionView!
    @IBOutlet weak var fullScreenImageSliderPageController: UIPageControl!
    
    @IBOutlet weak var menuImageFullScreenView: UIView!
    @IBOutlet weak var scrollViewForMenuImageView: UIScrollView!
    @IBOutlet weak var menuImageView: UIImageView!
    
    @IBOutlet weak var rightNavBarButton: UIBarButtonItem!
    

    var displayingSpaceId: Int!
    
    var displayingSpace: Space?
    var displayingSpaceDataStructure: NSMutableDictionary = [:]
    var displayingSpaceAvailabilityModules: NSMutableArray = []
    var displayingSpaceBlockAvailability: [[String : String]] = []
    var isAMySpace: Bool = false
    var areSimilarSpacesLoaded = false
    
    let COLLECTION_VIEW_SPECIAL_IDENTIFIERS = (coverImageCV: 0, eventTypeCV: 10, priceCV: 20, menusCV: 30, amenityCV: 41, extraAmenityCV: 42, seatingArrangementCV: 50, similarSpacesCV: 90, fullScreenImageCV: 99)
    
    var similarSpacesSectionHeight: CGFloat!
    var footer: UIView!
    
    var locationMapView: GMSMapView?
    var locationCameraPosition: GMSCameraPosition?
    
    var lastLoadedSection: Int = 0
    
    var alertController: UIAlertController?

    override func viewDidLoad() {
        super.viewDidLoad()
        //print("SpaceDetailsVC viewDidLoad")
        
        self.fullScreenImageSliderView.isHidden = true
        self.menuImageFullScreenView.isHidden = true
        self.menuImageView.isUserInteractionEnabled = false

        similarSpacesSectionHeight = CGFloat((UIScreen.main.bounds.width / 32) * 39)
        
        footer = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:10))
        footer.backgroundColor = UIColor.init(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)

        self.fullScreenImageCollectionView.tag = 99
        self.fullScreenImageCollectionView.isPagingEnabled = true
        self.fullScreenImageSliderPageController.isUserInteractionEnabled = false
        
        //self.constBookNowViewBottom.constant = -50.0
        
        retrieveSpaceDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //print("SpaceDetailsVC viewWillAppear")
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToSpaceDetailsFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        if self.menuImageFullScreenView.isHidden {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIApplication.shared.isStatusBarHidden = false
        }
        else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            UIApplication.shared.isStatusBarHidden = true
        }
        
        self.constBookNowViewBottom.constant = -50.0
        
        if self.displayingSpace != nil {
            self.showHideSpaceBookNowButton()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //print("SpaceDetailsVC viewWillDisappear")
        NotificationCenter.default.removeObserver(self)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    func returnedToSpaceDetailsFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) {
            // (in case if disabled) Enable device orientation restriction and change orientation to portrait
            UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
            self.setWindowOrientationRestriction(restriction: true)
            
            // (if shown) hide any alerts
            self.alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            
            // show nav bar and status bar (if hidden while fullscreen image views are open)
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            UIApplication.shared.isStatusBarHidden = false
            
            self.popToRootViewController()
        }
    }
    
    func retrieveSpaceDetails() {
        self.showActivityIndicator()
        
        WebServiceCall.retrieveSpace(spaceId: self.displayingSpaceId!, successBlock: { (result: Any) in
            //print("SpaceDetailsVC retrieveSpaceDetails | success result : \(result)")
            
            if let _results = result as? [String : Any] {
                self.displayingSpace = Space()
                self.displayingSpace!.initSpace(withDictionary: _results)
                
                DispatchQueue.main.sync(execute: {
                    self.configureView()
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("SpaceDetailsVC retrieveSpaceDetails | failure errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpaceDetailsReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
        
    }
    
    
    func retrieveSimilarSpacesForSpaceDisplayingSpace() {
        
        var eventTypeIDs: [Int] = []
        
        if FilterManager.spaceFilter.eventTypes.count > 0 {
            let displaySpecEventTypesSet: Set = Set.init(self.displayingSpace!.eventTypes as! [EventType])
            let filteredEventTypesSet = Set.init(FilterManager.spaceFilter.eventTypes)
            let eventTypeIntersection: Set = displaySpecEventTypesSet.intersection(filteredEventTypesSet)
            eventTypeIDs = Utils.getEventTypeIDs(eventTypes: [EventType](eventTypeIntersection))
        }
        else {
            eventTypeIDs = Utils.getEventTypeIDs(eventTypes: self.displayingSpace!.eventTypes as! [EventType])
        }
        
        WebServiceCall.retrieveSimilarSpacesForSpace(withSpaceID: self.displayingSpaceId!, eventTypeIDs: eventTypeIDs, successBlock: { (result: Any) in
            //print("SpaceDetailsVC retrieveSimilarSpacesForSpaceDisplayingSpace | success result : \(result)")
            
            if let similarSpaces = result as? [[String : Any]] {
                self.displayingSpace!.addSimilarSpaces(withSimilarSpacesArray: similarSpaces)
            }
            
            DispatchQueue.main.sync(execute: {
                self.configureViewForLoadedSimilarSpaces()
            })
        })
        { (errorCode: Int, error: String) in
            print("SpaceDetailsVC retrieveSimilarSpacesForSpaceDisplayingSpace | failure errorCode : \(errorCode) | error : \(error)")
        }
    }
    
    func addSpaceDetailsReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.retrieveSpaceDetails()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    func configureView() {
        // Configuring view for the available data of displaying Space
        
        self.showHideSpaceBookNowButton()
        
        var section: Int = 0
    
        // S1 - Cover Images ---> Mandatory
        var s1 = SpaceDataStructureForTableViewSection()
        s1.section = section
        s1.sectionSpecialIdentifier = section // 0
        s1.numOfRows = 1        // Images
        
        displayingSpaceDataStructure.setValue(s1, forKey: section.description)
        //print("SpaceDetailsVC configureView | S1 : Cover Images  - section : \(section), sectionSpecialIdentifier : \(s1.sectionSpecialIdentifier), numOfRows : \(s1.numOfRows)")
        
        
        // S2 - Space Primary Details ---> Mandatory
        section += 1
        var s2 = SpaceDataStructureForTableViewSection()
        s2.section = section
        s2.sectionSpecialIdentifier = 10
        s2.numOfRows = 3        // Title + Overview + EventTypes
        
        displayingSpaceDataStructure.setValue(s2, forKey: section.description)
        //print("SpaceDetailsVC configureView | S2 : Space Primary Details - section : \(section), sectionSpecialIdentifier : \(s2.sectionSpecialIdentifier), numOfRows : \(s2.numOfRows)")
        
        
        // S3 - Price ---> Mandatory
        section += 1
        var s3 = SpaceDataStructureForTableViewSection()
        s3.section = section
        s3.sectionSpecialIdentifier = 20
        s3.numOfRows = 2        // Title + Block/Hour Units
        
        displayingSpaceDataStructure.setValue(s3, forKey: section.description)
        //print("SpaceDetailsVC configureView | S3 : Price - section : \(section), sectionSpecialIdentifier : \(s3.sectionSpecialIdentifier), numOfRows : \(s3.numOfRows)")
        
        
        // S4 - Menus ---> Optional // TODO: 
        // if
        if displayingSpace!.menus.count > 0 {
            section += 1
            var s4 = SpaceDataStructureForTableViewSection()
            s4.section = section
            s4.sectionSpecialIdentifier = 30
            s4.numOfRows = 2        // Title + Images
            
            displayingSpaceDataStructure.setValue(s4, forKey: section.description)
            //print("SpaceDetailsVC configureView | S4 : Menus - section : \(section), sectionSpecialIdentifier : \(s4.sectionSpecialIdentifier), numOfRows : \(s4.numOfRows)")
            //
            
            //for m in displayingSpace!.menus {
                //print("SpaceDetailsVC configureView | S4: Menus - \(m.id, m.menuName!)")
            //}
        }
        else {
            //print("SpaceDetailsVC configureView | S4 : Space Menus NOT AVAILABLE")
        }
        

        
        // S5 - Amenities ---> Mandatory
        if displayingSpace!.amenities.count > 0 || displayingSpace!.extraAmenities.count > 0 {
            
            section += 1
            
            var s5 = SpaceDataStructureForTableViewSection()
            s5.section = section
            s5.sectionSpecialIdentifier = 40
            
            s5.numOfRows = 1        // Title
            s5.numOfRows = (displayingSpace!.amenities.count > 0) ? s5.numOfRows + 1 : s5.numOfRows + 0 // Complementary
            s5.numOfRows = (displayingSpace!.extraAmenities.count > 0) ? s5.numOfRows + 1 : s5.numOfRows + 0 // Chargable
            
            displayingSpaceDataStructure.setValue(s5, forKey: section.description)
            //print("SpaceDetailsVC configureView | S5 : Amenities - section : \(section), sectionSpecialIdentifier : \(s5.sectionSpecialIdentifier), numOfRows : \(s5.numOfRows)")
        }
        else {
            //print("SpaceDetailsVC configureView | S5 : Amenities NOT AVAILABLE")
        }
        
        
        // S6 - Seating Arrangements ---> Optional
        if displayingSpace!.seatingArrangements.count > 0 {
            
            section += 1
            
            var s6 = SpaceDataStructureForTableViewSection()
            s6.section = section
            s6.sectionSpecialIdentifier = 50
            s6.numOfRows = 2        // Title + Seating Arrangements
            
            displayingSpaceDataStructure.setValue(s6, forKey: section.description)
            //print("SpaceDetailsVC configureView | S6 : Seating Arrangements - section : \(section), sectionSpecialIdentifier : \(s6.sectionSpecialIdentifier), numOfRows : \(s6.numOfRows)")
        }
        else {
            //print("SpaceDetailsVC configureView | S6 : Seating Arrangements NOT AVAILABLE")
        }
        
        
        // S7 : Space Rules ---> Optional
        if displayingSpace!.spaceRules.count > 0 {
            
            section += 1
            
            var s7 = SpaceDataStructureForTableViewSection()
            s7.section = section
            s7.sectionSpecialIdentifier = 60
            s7.numOfRows = displayingSpace!.spaceRules.count + 1   // Title + Rules Count
            
            displayingSpaceDataStructure.setValue(s7, forKey: section.description)
            //print("SpaceDetailsVC configureView | S7 : Space Rules - section : \(section), sectionSpecialIdentifier : \(s7.sectionSpecialIdentifier), numOfRows : \(s7.numOfRows)")
        }
        else {
            //print("SpaceDetailsVC configureView | S7 : Space Rules NOT AVAILABLE")
        }
        
        
        // S8 : Cancellation Policy ---> Mandatory
        section += 1
        var s8 = SpaceDataStructureForTableViewSection()
        s8.section = section
        s8.sectionSpecialIdentifier = 70
        s8.numOfRows = 1        // Cancellation Policy
        
        displayingSpaceDataStructure.setValue(s8, forKey: section.description)
        //print("SpaceDetailsVC configureView | S8 : Cancellation Policy - section : \(section), sectionSpecialIdentifier : \(s8.sectionSpecialIdentifier), numOfRows : \(s8.numOfRows)")
        
        
        // S9 : Location ---> Mandatory
        section += 1
        var s9 = SpaceDataStructureForTableViewSection()
        s9.section = section
        s9.sectionSpecialIdentifier = 80
        s9.numOfRows = 2        // Title + Map
        
        displayingSpaceDataStructure.setValue(s9, forKey: section.description)
        //print("SpaceDetailsVC configureView | S9 : Location - section : \(section), sectionSpecialIdentifier : \(s9.sectionSpecialIdentifier), numOfRows : \(s9.numOfRows)")
        
    
        lastLoadedSection = section
        
        /* Currently Loaded Separately
        // S10 : Similar Spaces ---> Optional
        if displayingSpace!.similarSpaces.count > 0 {
            section += 1
            var s10 = SpaceDataStructureForTableViewSection()
            s10.section = section
            s10.sectionSpecialIdentifier = 90
            s10.numOfRows = 1        // Similar Spaces
            
            displayingSpaceDataStructure.setValue(s10, forKey: section.description)
            //print("SpaceDetailsVC configureView | S10 : Similar Spaces - section : \(section), sectionSpecialIdentifier : \(s10.sectionSpecialIdentifier), numOfRows : \(s10.numOfRows)")
        }
        else{
            //print("SpaceDetailsVC configureView | S10 : Similar Spaces NOT AVAILABLE")
        }*/
        
        //print("SpaceDetailsVC configureView | space amenities : \(String(describing: displayingSpace!.amenities))")
        //print("SpaceDetailsVC configureView | space extraAmenities : \(String(describing: displayingSpace!.extraAmenities))")
        //print("SpaceDetailsVC configureView | space futureBookings : \(String(describing: displayingSpace!.futureBookings))")


        // Executing other configurations
        configureSpacePricing()
        configureLocationMapView()
        
        self.spaceDetailsTableView.reloadData()
        self.spaceDetailsTableView.delaysContentTouches = true
        //self.locationMapView?.isUserInteractionEnabled = false
        
        self.fullScreenImageSliderPageController.numberOfPages = self.displayingSpace!.images.count
        self.fullScreenImageCollectionView.reloadData()
    }
    
    func configureViewForLoadedSimilarSpaces() {
        
        // S10 : Similar Spaces ---> Optional
        if displayingSpace!.similarSpaces.count > 0 {
            lastLoadedSection += 1
            var s10 = SpaceDataStructureForTableViewSection()
            s10.section = lastLoadedSection
            s10.sectionSpecialIdentifier = 90
            s10.numOfRows = 1        // Similar Spaces
            
            displayingSpaceDataStructure.setValue(s10, forKey: lastLoadedSection.description)
            //print("SpaceDetailsVC configureView | S10 : Similar Spaces - section : \(section), sectionSpecialIdentifier : \(s10.sectionSpecialIdentifier), numOfRows : \(s10.numOfRows)")
        }
        else{
            //print("SpaceDetailsVC configureView | S10 : Similar Spaces NOT AVAILABLE")
        }
        
        self.spaceDetailsTableView.reloadData()
    }
    
    
    func configureSpacePricing() {
        if displayingSpace!.availabilityMethod == "BLOCK_BASE" {
            //displayingSpaceAvailabilityModules = displayingSpace!.availabilityModules
            displayingSpaceBlockAvailability = StaticSpaceFunctions.prepareBlockPricesForSpaceDetailsScreen(spaceAvailability: displayingSpace!.availabilityModules as! [SpaceAvailabilityModule], blockChargeType: displayingSpace!.blockChargeType)
            print("SpaceDetailsVC configureSpacePricing | displayingSpaceBlockAvailability: \(displayingSpaceBlockAvailability)")
        }
        else {
            displayingSpaceAvailabilityModules = displayingSpace!.availabilityModules.sorted(by: {($0 as! SpaceAvailabilityModule).day < ($1 as! SpaceAvailabilityModule).day}) as! NSMutableArray
        }
    }
    
    
    func configureLocationMapView() {
        self.locationCameraPosition = GMSCameraPosition.camera(withLatitude: displayingSpace!.latitude,
                                              longitude: displayingSpace!.longitude,
                                              zoom: 16)
        let mapViewWidth = UIScreen.main.bounds.width - 20
        let mapViewHeight = (mapViewWidth/30) * 17
        let mapViewSize = CGRect(x: 0, y: 0, width: mapViewWidth, height: mapViewHeight)
        
        self.locationMapView = GMSMapView.map(withFrame: mapViewSize, camera: self.locationCameraPosition!)
        
        let marker = GMSMarker()
        marker.position = self.locationCameraPosition!.target
        marker.appearAnimation = .pop
        marker.map = self.locationMapView
        
        //self.locationMapView!.isExclusiveTouch = true
        //self.locationMapView?.settings.consumesGesturesInView = false
        //self.locationMapView?.settings.scrollGestures = false
    }
    
    private func showHideSpaceBookNowButton() {
        if AuthenticationManager.isUserAuthenticated {
            isAMySpace = (AuthenticationManager.user?.id == displayingSpace!.hostId)
            if isAMySpace {
                self.bookNowButton.isUserInteractionEnabled = false
                self.constBookNowViewBottom.constant = -50.0
            }
            else {
                self.bookNowButton.isUserInteractionEnabled = true
                self.constBookNowViewBottom.constant = 0.0
            }
        }
        else {
            self.bookNowButton.isUserInteractionEnabled = true
            self.constBookNowViewBottom.constant = 0.0
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.menuImageFullScreenView) else { return }
        if menuImageFullScreenView.frame.contains(location) {
            self.showFullScreenImageViewForMenuImages(show: false, menuImageName: "")
        }
    }
    
    func showFullScreenImageSliderView(show: Bool) {
        self.navigationController?.setNavigationBarHidden(show, animated: true)
        UIApplication.shared.isStatusBarHidden = show
        self.fullScreenImageSliderView.isHidden = !show
        
        if show {
            self.setWindowOrientationRestriction(restriction: false)
            self.fullScreenImageCollectionView.reloadData()
            //UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.landscapeRight.rawValue), forKey: "orientation")
        }
        else {
            UIDevice.current.setValue(NSNumber(value: UIInterfaceOrientation.portrait.rawValue), forKey: "orientation")
            self.setWindowOrientationRestriction(restriction: true)
        }
    }
    
    func showFullScreenImageViewForMenuImages(show: Bool, menuImageName: String?) {
        self.navigationController?.setNavigationBarHidden(show, animated: true)
        UIApplication.shared.isStatusBarHidden = show
        self.menuImageFullScreenView.isHidden = !show
        
        UIView.transition(with: self.menuImageFullScreenView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.menuImageFullScreenView.isHidden = !show
        }, completion: nil)
        
        if show {
            self.scrollViewForMenuImageView.setZoomScale(1.0, animated: false)
            self.scrollViewForMenuImageView.minimumZoomScale = self.scrollViewForMenuImageView.frame.size.width / self.menuImageView.frame.size.width
            self.scrollViewForMenuImageView.maximumZoomScale = 5.0
            self.scrollViewForMenuImageView.contentSize = self.menuImageView.frame.size
            
            self.menuImageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + menuImageName!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            
            let doubleTapToZoom = UITapGestureRecognizer.init(target: self, action: #selector(handleDoubleTapToZoomMenuImage(_:)))
            doubleTapToZoom.numberOfTapsRequired = 2
            self.scrollViewForMenuImageView.addGestureRecognizer(doubleTapToZoom)
        }
        else {
            self.menuImageView.image = nil
        }
    }
    
    
    
    func handleDoubleTapToZoomMenuImage(_ gestureRecognizer: UITapGestureRecognizer) {
        print("SpaceDetailsVC handleDoubleTapToZoomMenuImage")
        
        if self.scrollViewForMenuImageView.zoomScale > self.scrollViewForMenuImageView.minimumZoomScale {
            self.scrollViewForMenuImageView.setZoomScale(1.0, animated: true)
        }
        else {
            let tappedLocation = gestureRecognizer.location(in: gestureRecognizer.view)
            
            let w = self.scrollViewForMenuImageView.frame.size.width / self.scrollViewForMenuImageView.maximumZoomScale
            let h = self.scrollViewForMenuImageView.frame.size.height / self.scrollViewForMenuImageView.maximumZoomScale
            let x = tappedLocation.x - (w/2.0)
            let y = tappedLocation.y - (h/2.0)
            
            let rectToZoom = CGRect.init(x: x, y: y, width: w, height: h)
            self.scrollViewForMenuImageView.zoom(to: rectToZoom, animated: true)
        }
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if (size.width / size.height > 1) {
            print("landscape")
        } else {
            print("portrait")
        }
        
        if UIDevice.current.orientation.isLandscape {
            print("SpaceDetailsVC viewWillTransition | isLandscape")
            //print("SpaceDetailsVC viewWillTransition | isLandscape | screen width : \(UIScreen.main.bounds.width)")
            //print("SpaceDetailsVC viewWillTransition | isLandscape | screen height : \(UIScreen.main.bounds.height)")
        }
        else {
            print("SpaceDetailsVC viewWillTransition | isPortrait")
            //print("SpaceDetailsVC viewWillTransition | isPortrait | screen width : \(UIScreen.main.bounds.width)")
            //print("SpaceDetailsVC viewWillTransition | isPortrait | screen height : \(UIScreen.main.bounds.height)")
        }
        
        if !self.fullScreenImageSliderView.isHidden {
            self.fullScreenImageCollectionView.reloadData()
        }
    }
    
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if let space = displayingSpace {
            var message = "Check out \(space.name!)"
            if let add2 = space.addressLine2 {
                message.append(" at \(add2)!")
            }
            else {
                message.append("!")
            }
            var spaceName = space.name!.trimmingCharacters(in: .whitespaces)
            spaceName = spaceName.replacingOccurrences(of: " ", with: "-")
            let spaceUrl = URL(string: "\(Configuration.MAIN_WEBSITE_DOMAIN_FOR_API_ENVIRONMENT)/#/spaces/\(space.id!)/\(spaceName)")

            let activityViewController = UIActivityViewController.init(activityItems: [message, spaceUrl!], applicationActivities: nil)
            navigationController?.present(activityViewController, animated: true, completion: nil)
        }
    }
    

    @IBAction func bookNowButtonPressed(_ sender: UIButton) {
        //performSegue(segueID: "BookingCalendarSegue")
        if displayingSpace != nil {
            if AuthenticationManager.isUserAuthenticated {
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let bookingInitialVC = storyboard.instantiateViewController(withIdentifier: "BookingInitialVC") as! BookingInitialViewController
                bookingInitialVC.bookingSpace = displayingSpace!
                self.navigationController?.pushViewController(bookingInitialVC, animated: true)
            }
            else {
                self.openSignInViewController()
            }
        }
    }

    
    @IBAction func menuFullScreenViewCloseButtonPressed(_ sender: UIButton) {
        self.showFullScreenImageViewForMenuImages(show: false, menuImageName: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

// MARK: - UIScrollViewDelegate
extension SpaceDetailsViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.spaceDetailsTableView {
            if !isAMySpace {
                self.constBookNowViewBottom.constant = -50.0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView == self.spaceDetailsTableView {
            if !isAMySpace {
                self.constBookNowViewBottom.constant = 0.0
                UIView.animate(withDuration: 0.3, delay: 0.5, options: UIViewAnimationOptions.allowUserInteraction, animations: {
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV {
            print("SpaceDetailsVC scrollViewDidScroll | coverImageCV scrolls")
            if let coverImageCVEmbeddedCell = self.spaceDetailsTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CollectionViewEmbeddedTableViewCell {
                coverImageCVEmbeddedCell.pageController.currentPage = Int((coverImageCVEmbeddedCell.collectionView.contentOffset.x + UIScreen.main.bounds.width / 2) / UIScreen.main.bounds.width)
            }
        }
        else if scrollView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV {
            print("SpaceDetailsVC scrollViewDidScroll | fullScreenImageCV scrolls")
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.menuImageView
    }
}

// MARK: - UITableView DataSource and Delegate
extension SpaceDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if displayingSpace != nil {
            return displayingSpaceDataStructure.count
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == displayingSpaceDataStructure.count - 1 {
            return 0.01
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if displayingSpace != nil {
            return numberRowsInTableViewSection(section: section)
        }
        else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAtIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightForRowAtIndexPath(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if displayingSpace != nil {
            return populateTableViewCell(forTableView: tableView, forIndexPath: indexPath)
        }
        else {
            return UITableViewCell()
        }
    }
    
    /*
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }*/
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == displayingSpaceDataStructure.count - 1 && !self.areSimilarSpacesLoaded {
            self.areSimilarSpacesLoaded = true
            self.retrieveSimilarSpacesForSpaceDisplayingSpace()
        }
    }
}

// MARK: - UITableViewDataSource Helper functions
extension SpaceDetailsViewController {
    
    func numberRowsInTableViewSection(section: Int) -> Int {
        let sectionDataStructure = self.displayingSpaceDataStructure[section.description] as! SpaceDataStructureForTableViewSection
        return sectionDataStructure.numOfRows
    }
    
    func heightForRowAtIndexPath(indexPath: IndexPath) -> CGFloat{
        let sectionDataStructure = self.displayingSpaceDataStructure[indexPath.section.description] as! SpaceDataStructureForTableViewSection
        
        switch sectionDataStructure.sectionSpecialIdentifier {
        case 0:
            // Cover Image
            return CGFloat((UIScreen.main.bounds.width / 32) * 22)
            
        case 10:
            // Space Primary Details
            return indexPath.row == 2 ? 85 : UITableViewAutomaticDimension
            
        case 20:
            // Price
            if displayingSpace!.availabilityMethod == "BLOCK_BASE" {
                if indexPath.row == 0 {
                    return UITableViewAutomaticDimension
                }
                else {
                    return displayingSpace!.blockChargeType == 2 ? 150 : 125
                }
            }
            else {
                return indexPath.row == 0 ? UITableViewAutomaticDimension : 100
            }
            
        case 30:
            // Menus
            return indexPath.row == 0 ? 50 : 130
            
        case 40:
            // Amenities
            if indexPath.row == 0 {
                return 50
            }
            else {
                if sectionDataStructure.numOfRows == 3 {
                    if indexPath.row == 1 {
                        return 115 //85 // Complementary
                    }
                    else { // if indexPath.row == 2
                        return 165 //135 // Chargable
                    }
                }
                else {
                    if displayingSpace!.amenities.count > 0 {
                        return 115 // Complementary
                    }
                    else {
                        return 165 // Chargable
                    }
                }
            }

        case 50:
            // Seating Arrangements
            return indexPath.row == 0 ? 50 : Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: displayingSpace!.seatingArrangements.count, cvCellHeight: 50, itemsPerRow: 2)
            
        case 60:
            // Space Rules
            return indexPath.row == 0 ? 50 : 30
            
        case 70:
            // Cancellation Policy
            return UITableViewAutomaticDimension
            
        case 80:
            // Space Location
            return indexPath.row == 0 ? 50 : UITableViewAutomaticDimension
            
        case 90:
            // SimilarSpacesCVECell
            return similarSpacesSectionHeight
            
        default:
            return 0
        }
        
        //return 0
    }

    func populateTableViewCell(forTableView tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
        
        let sectionDataStructure = self.displayingSpaceDataStructure[indexPath.section.description] as! SpaceDataStructureForTableViewSection
        
        switch sectionDataStructure.sectionSpecialIdentifier {
        case 0:
            // Cover Image
            let coverImagesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CoverImageHeaderCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            coverImagesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
            return coverImagesEmbeddedCell
        
        case 10:
            // Space Primary Details
            switch indexPath.row {
            case 0:
                // Title
                let titleCell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath)
                (titleCell.viewWithTag(1) as! UILabel).text = displayingSpace!.name
                (titleCell.viewWithTag(2) as! UILabel).text = displayingSpace?.fullAdrress!
                
                let ratingView = titleCell.viewWithTag(3) as! StarRatingView
                ratingView.setRatingValue(withFloat: 3.5)
                for c in ratingView.constraints {
                    if c.identifier == "srW" {
                        c.constant = 0
                        // TODO:
                    }
                }
                return titleCell
                
            case 1:
                // Overview
                let overviewCell = tableView.dequeueReusableCell(withIdentifier: "OverviewCell", for: indexPath)
                (overviewCell.viewWithTag(1) as! UILabel).text = displayingSpace!.sDescription
                (overviewCell.viewWithTag(2) as! UILabel).text = "\((displayingSpace!.participantCount)!) Guests"
                (overviewCell.viewWithTag(3) as! UILabel).text = "\((displayingSpace!.size)!) \((displayingSpace!.sizeMeasurementUnit.name)!)"
                
                if let spaceType = self.displayingSpace!.spaceType {
                    (overviewCell.viewWithTag(4) as! UIImageView).sd_setImage(with: URL(string: spaceType.spaceTypeIconUrlStr!), completed: nil)
                    (overviewCell.viewWithTag(5) as! UILabel).text = spaceType.spaceTypeName!
                }
                else {
                    (overviewCell.viewWithTag(4) as! UIImageView).image = nil
                    (overviewCell.viewWithTag(5) as! UILabel).text = ""
                }
                
                return overviewCell
                
            case 2:
                // EventTypes
                let eventTypesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CVECellOne", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                eventTypesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return eventTypesEmbeddedCell
                
            default:
                return UITableViewCell()
            }
        case 20:
            // Price
            switch indexPath.row {
            case 0:
                // Title
                let priceHeaderCell = tableView.dequeueReusableCell(withIdentifier: "PriceHeaderCell", for: indexPath)
                let availabilityMethodLabel = priceHeaderCell.viewWithTag(1) as! UILabel
                if displayingSpace!.availabilityMethod == "BLOCK_BASE" {
                    if displayingSpace!.blockChargeType == 1 {
                        if displayingSpace!.hasReimbursableBlocks {
                            availabilityMethodLabel.text = "The host charges on a per block basis. The below payment can be reimbursed against your food/beverage order on the day of the event."
                        }
                        else {
                            availabilityMethodLabel.text = "The host charges on a per block basis. This is a space hiring charge only."
                        }
                    }
                    else if displayingSpace!.blockChargeType == 2 {
                        availabilityMethodLabel.text = "The host charges on a per block basis. You will be paying based on the number of guests attending the event. \n \nMinimum number of guests applicable - \(displayingSpace!.minimumParticipantCount)"
                    }
                }
                else {
                    availabilityMethodLabel.text = "The host charges on a per hour basis."
                }
                
                return priceHeaderCell
                
            case 1:
                // Price Units
                let priceUnitsEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CVECellOne", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                priceUnitsEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return priceUnitsEmbeddedCell
                
            default:
                return UITableViewCell()
            }
        case 30:
            // Menus
            switch indexPath.row {
            case 0:
                // Title
                let menuHeaderCell = tableView.dequeueReusableCell(withIdentifier: "OtherHeaderCell", for: indexPath)
                (menuHeaderCell.viewWithTag(1) as! UILabel).text = "Menu Options"
                return menuHeaderCell
                
            case 1:
                // Price Units
                let menusEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CVECellOne", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                menusEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return menusEmbeddedCell
                
            default:
                return UITableViewCell()
            }
        case 40:
            // Amenities
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherHeaderCell", for: indexPath)
                (cell.viewWithTag(1) as! UILabel).text = "Amenities"
                return cell
            }
            else {
                let amenityEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "AmenitiesCVECell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                let amenityTypeLabel = amenityEmbeddedCell.viewWithTag(1) as! UILabel
                
                if sectionDataStructure.numOfRows == 3 {
                    if indexPath.row == 1 {
                        amenityTypeLabel.text = "Complimentary"
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 1))
                    }
                    else { //if indexPath.row == 2
                        amenityTypeLabel.text = "Chargeable"
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 2))
                    }
                }
                else {
                    if displayingSpace!.amenities.count > 0 {
                        amenityTypeLabel.text = "Complimentary"
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 1))
                    }
                    else {
                        amenityTypeLabel.text = "Chargeable"
                        amenityEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: (sectionDataStructure.sectionSpecialIdentifier + 2))
                    }
                }
                
                return amenityEmbeddedCell
            }
        case 50:
            // Seating Arrangements
            switch indexPath.row {
            case 0:
                // Title
                let seatingArrangementHeaderCell = tableView.dequeueReusableCell(withIdentifier: "OtherHeaderCell", for: indexPath)
                (seatingArrangementHeaderCell.viewWithTag(1) as! UILabel).text = "Seating Arrangements"
                return seatingArrangementHeaderCell
                
            case 1:
                // Seating Arrangements
                let seatingArrangementsEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "CVECellTwo", for: indexPath) as! CollectionViewEmbeddedTableViewCell
                seatingArrangementsEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
                return seatingArrangementsEmbeddedCell
                
            default:
                return UITableViewCell()
            }
        case 60:
            // Space Rules
            if indexPath.row == 0 {
                let ruleHeaderCell = tableView.dequeueReusableCell(withIdentifier: "OtherHeaderCell", for: indexPath)
                (ruleHeaderCell.viewWithTag(1) as! UILabel).text = "Space Rules"
                return ruleHeaderCell
            }
            else {
                let ruleCell = tableView.dequeueReusableCell(withIdentifier: "SpaceRuleCell", for: indexPath)
                (ruleCell.viewWithTag(1) as! UILabel).text = (displayingSpace!.spaceRules[indexPath.row-1] as! Rule).name
                return ruleCell
            }
            
        case 70:
            // Cancellation Policy
            let cPolicyCell = tableView.dequeueReusableCell(withIdentifier: "cPolicyCell", for: indexPath)
            (cPolicyCell.viewWithTag(1) as! UILabel).text = displayingSpace!.cancellationPolicy.name
            (cPolicyCell.viewWithTag(2) as! UILabel).text = displayingSpace!.cancellationPolicy.policyDescription
            return cPolicyCell
            
        case 80:
            // Space Location
            if indexPath.row == 0 {
                let locationHeaderCell = tableView.dequeueReusableCell(withIdentifier: "OtherHeaderCell", for: indexPath)
                (locationHeaderCell.viewWithTag(1) as! UILabel).text = "Location"
                return locationHeaderCell
            }
            else {
                let locationCell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath)
                // TODO: Map View
                
                let subViewForMap = locationCell.viewWithTag(1)!
                subViewForMap.autoresizesSubviews = true
                self.locationMapView!.animate(to: self.locationCameraPosition!)
                subViewForMap.addSubview(self.locationMapView!)
                return locationCell
            }
            
        case 90:
            // SimilarSpacesCVECell
            let similarSpacesEmbeddedCell = tableView.dequeueReusableCell(withIdentifier: "SimilarSpacesCVECell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            similarSpacesEmbeddedCell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: sectionDataStructure.sectionSpecialIdentifier)
            return similarSpacesEmbeddedCell
            
        default:
            return UITableViewCell()
        }
    }
    
}



// MARK: - UICollectionViewDataSource
extension SpaceDetailsViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if displayingSpace != nil {
            return numberOfItemsInCollectionView(collectionView: collectionView)
        }
        else {
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if displayingSpace != nil {
            return populateCollectionViewCell(forCollectionView: collectionView, forIndexPath: indexPath)
        }
        else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV {
            self.fullScreenImageCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
        else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV {
            self.fullScreenImageSliderPageController.currentPage = indexPath.item
            
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if displayingSpace != nil {
            if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.similarSpacesCV {
                let similarSpaceAtItem = displayingSpace!.similarSpaces[indexPath.item] as! Space
                let spaceDetailsController = self.storyboard?.instantiateViewController(withIdentifier: "SpaceDetailsVC") as! SpaceDetailsViewController
                spaceDetailsController.displayingSpaceId = similarSpaceAtItem.id
                self.navigationController?.pushViewController(spaceDetailsController, animated: true)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV {
                self.showFullScreenImageSliderView(show: true)
                collectionView.deselectItem(at: indexPath, animated: false)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV {
                self.showFullScreenImageSliderView(show: false)
            }
            else if collectionView.tag == COLLECTION_VIEW_SPECIAL_IDENTIFIERS.menusCV {
                self.showFullScreenImageViewForMenuImages(show: true, menuImageName: (displayingSpace!.menus[indexPath.item]).menuImage!)
                collectionView.deselectItem(at: indexPath, animated: false)
            }
        }
    }
}

extension SpaceDetailsViewController: UICollectionViewDelegateFlowLayout {
    
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if displayingSpace != nil {
            return collectionViewCellLayoutSize(forCollectionView: collectionView)
        }
        else {
            return CGSize()
        }
    }
    
    /*
     //3
     func collectionView(_ collectionView: UICollectionView,
     layout collectionViewLayout: UICollectionViewLayout,
     insetForSectionAt section: Int) -> UIEdgeInsets {
     return sectionInsets
     }
     
     // 4
     func collectionView(_ collectionView: UICollectionView,
     layout collectionViewLayout: UICollectionViewLayout,
     minimumLineSpacingForSectionAt section: Int) -> CGFloat {
     return sectionInsets.left
     }*/
    
}

// MARK: - UICollectionViewDataSource and DelegateFlowLayout Helper functions
extension SpaceDetailsViewController {
    
    func numberOfItemsInCollectionView(collectionView: UICollectionView) -> Int {
        switch collectionView.tag {
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV:
            return displayingSpace!.images.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.eventTypeCV:
            return displayingSpace!.eventTypes.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.priceCV:
            return (self.displayingSpace!.blockChargeType == 0) ? displayingSpaceAvailabilityModules.count : displayingSpaceBlockAvailability.count
        
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.menusCV:
            return displayingSpace!.menus.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.amenityCV:
            return displayingSpace!.amenities.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.extraAmenityCV:
            return displayingSpace!.extraAmenities.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.seatingArrangementCV:
            return displayingSpace!.seatingArrangements.count
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.similarSpacesCV:
            return displayingSpace!.similarSpaces.count
           
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV:
            return displayingSpace!.images.count
            
        default:
            return 0
        }
    }
    
    func populateCollectionViewCell(forCollectionView collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV:
            let coverImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoverImageCell", for: indexPath)
            collectionView.isPagingEnabled = true
            coverImageCell.preservesSuperviewLayoutMargins = false
            let imageView = coverImageCell.viewWithTag(1) as! UIImageView
            //imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), completed: nil)
            return coverImageCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.eventTypeCV:
            let eventTypeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventTypeCell", for: indexPath)
            let eventTypeAtItem = displayingSpace!.eventTypes[indexPath.item] as! EventType
            //(eventTypeCell.viewWithTag(1) as! UIImageView).imageFromServerURL(urlString: eventTypeAtItem.iconUrl!)
            let imageView = eventTypeCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: eventTypeAtItem.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            (eventTypeCell.viewWithTag(2) as! UILabel).text = eventTypeAtItem.name!
            return eventTypeCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.priceCV:
            if displayingSpace!.availabilityMethod == "BLOCK_BASE" { // Block Base
                
                //let blockRateAtItem = displayingSpaceAvailabilityModules[indexPath.item] as! SpaceAvailabilityModule
                let newBlockAtItem = displayingSpaceBlockAvailability[indexPath.item]
                
                let blockRateCell: UICollectionViewCell!
                if displayingSpace!.blockChargeType == 2 {
                    blockRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlockRateMenuCell", for: indexPath)
                    //(blockRateCell.viewWithTag(4) as! UILabel).text = (blockRateAtItem.blockMenus[0]).menuName!
                    (blockRateCell.viewWithTag(4) as! UILabel).text = newBlockAtItem["menu"]!
                }
                else {
                    blockRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "BlockRateCell", for: indexPath)
                }
                
                //(blockRateCell.viewWithTag(1) as! UILabel).text = "\(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: blockRateAtItem.timeFrom!, isWithSeconds: true)) - \(CalendarUtils.convertTo12HourTime(fromTwentyFourTime: blockRateAtItem.timeTo!, isWithSeconds: true))"
                //(blockRateCell.viewWithTag(2) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: blockRateAtItem.charge))"
                //(blockRateCell.viewWithTag(3) as! UILabel).text = blockRateAtItem.dayDisplayName
                
                (blockRateCell.viewWithTag(1) as! UILabel).text = newBlockAtItem["time"]!
                (blockRateCell.viewWithTag(2) as! UILabel).text = newBlockAtItem["rate"]!
                (blockRateCell.viewWithTag(3) as! UILabel).text = newBlockAtItem["days"]!
                
                return blockRateCell
            }
            else {
                // Hour Base
                let hourRateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourRateCell", for: indexPath)
                let hourRateAtItem = displayingSpaceAvailabilityModules[indexPath.item] as! SpaceAvailabilityModule
                (hourRateCell.viewWithTag(1) as! UILabel).text = hourRateAtItem.dayDisplayName
                (hourRateCell.viewWithTag(3) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: hourRateAtItem.charge))"
                return hourRateCell
            }
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.menusCV:
            // TODO: Resolve Menus
            let menuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuOptionsCell", for: indexPath)
            let menuAtIndex = displayingSpace!.menus[indexPath.item]
            (menuCell.viewWithTag(1) as! UILabel).text = menuAtIndex.menuName!
            let imageView = menuCell.viewWithTag(2) as! UIImageView
            imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + menuAtIndex.menuImage!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            return menuCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.amenityCV:
            let amenityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AmenityCompCell", for: indexPath)
            let amenityAtItem = displayingSpace!.amenities[indexPath.item] as! Amenity
            //(amenityCell.viewWithTag(1) as! UIImageView).imageFromServerURL(urlString: amenityAtItem.iconUrl!)
            let imageView = amenityCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: amenityAtItem.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            (amenityCell.viewWithTag(2) as! UILabel).text = amenityAtItem.name!
            return amenityCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.extraAmenityCV:
            let extraAmenityCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AmenityChargCell", for: indexPath)
            let extraAmenityAtItem = displayingSpace!.extraAmenities[indexPath.item] as! SpaceExtraAmenity
            //(extraAmenityCell.viewWithTag(1) as! UIImageView).imageFromServerURL(urlString: extraAmenityAtItem.extraAmenity.iconUrl!)
            let imageView = extraAmenityCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: extraAmenityAtItem.extraAmenity.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            
            (extraAmenityCell.viewWithTag(2) as! UILabel).text = extraAmenityAtItem.extraAmenity.name!
            (extraAmenityCell.viewWithTag(4) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: extraAmenityAtItem.rate))"
            (extraAmenityCell.viewWithTag(5) as! UILabel).text = extraAmenityAtItem.extraAmenityUnit.name!
            
            return extraAmenityCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.seatingArrangementCV:
            let seatingArrangementCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeatingStyleCell", for: indexPath)
            
            let seatingAtItem = displayingSpace!.seatingArrangements[indexPath.item] as! SpaceSeatingArrangement
            //(seatingArrangementCell.viewWithTag(1) as! UIImageView).imageFromServerURL(urlString: seatingAtItem.arrangement.iconUrl!)
            let imageView = seatingArrangementCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: seatingAtItem.arrangement.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            (seatingArrangementCell.viewWithTag(2) as! UILabel).text = seatingAtItem.arrangement.name!
            (seatingArrangementCell.viewWithTag(3) as! UILabel).text = "Maximum \(seatingAtItem.participantCount!)"
            
            return seatingArrangementCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.similarSpacesCV:
            let similarSpaceCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarSpaceCVCell", for: indexPath)
            
            let similarSpaceAtItem = displayingSpace!.similarSpaces[indexPath.item] as! Space
            //(similarSpaceCell.viewWithTag(1) as! UIImageView).imageFromServerURL(urlString: Configuration.IMAGE_BASE_URL + similarSpaceAtItem.thumbnailImage!)
            let imageView = similarSpaceCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + similarSpaceAtItem.thumbnailImage!)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            //imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + similarSpaceAtItem.thumbnailImage!)), completed: nil)
            
            (similarSpaceCell.viewWithTag(2) as! UILabel).text = similarSpaceAtItem.name
            (similarSpaceCell.viewWithTag(3) as! UILabel).text = (similarSpaceAtItem.addressLine2 != nil) ? similarSpaceAtItem.addressLine2! : similarSpaceAtItem.name
            
            (similarSpaceCell.viewWithTag(4) as! UILabel).text = "LKR \(Utils.formatPriceValues(price: similarSpaceAtItem.ratePerHour!))"
            
            // TEMP:
            (similarSpaceCell.viewWithTag(5) as! StarRatingView).isHidden = true
            //(similarSpaceCell.viewWithTag(4) as! StarRatingView).setRatingValue(withFloat: 3.5)
            
            return similarSpaceCell
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV:
            let fullScreenImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FullScreenImageCell", for: indexPath)
            let imageView = fullScreenImageCell.viewWithTag(1) as! UIImageView
            imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
            //imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + (displayingSpace!.images[indexPath.item] as! String))), completed: nil)
            return fullScreenImageCell
            
        default:
            return UICollectionViewCell()
        }
    }

    
    func collectionViewCellLayoutSize(forCollectionView collectionView: UICollectionView) -> CGSize {
        switch collectionView.tag {
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.coverImageCV:
            let cellWidth = UIScreen.main.bounds.width
            let cellHeight = (cellWidth/32) * 22
            return CGSize(width: cellWidth, height: cellHeight)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.eventTypeCV:
            return CGSize(width: 85, height: 85)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.priceCV:
            if displayingSpace!.availabilityMethod == "BLOCK_BASE" {
                if displayingSpace!.blockChargeType == 2 {
                    return CGSize(width: 120, height: 150)
                }
                else {
                    return CGSize(width: 120, height: 125)
                }
            }
            else {
                return CGSize(width: 120, height: 100)
            }
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.menusCV:
            return CGSize(width: 70, height: 130)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.amenityCV:
            return CGSize(width: 100, height: 85)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.extraAmenityCV:
            return CGSize(width: 120, height: 135)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.seatingArrangementCV:
            let cellWidth = (UIScreen.main.bounds.width - 50) / 2
            return CGSize(width: cellWidth, height: 50)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.similarSpacesCV:
            let cellWidth = UIScreen.main.bounds.width - 70.0
            return CGSize(width: cellWidth, height: similarSpacesSectionHeight - 40)
            
        case COLLECTION_VIEW_SPECIAL_IDENTIFIERS.fullScreenImageCV:
            return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        default:
            return CGSize()
        }
    }
    
    /*
    override var shouldAutorotate: Bool {
    }*/
    
}
