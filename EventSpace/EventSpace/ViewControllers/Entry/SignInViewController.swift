//
//  SignInViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 8/22/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import CryptoSwift

import FBSDKCoreKit
import FBSDKLoginKit

import Firebase
import GoogleSignIn

class SignInViewController: BaseViewController, UITextFieldDelegate, GIDSignInUIDelegate {
    
    @IBOutlet weak var fbSignInButton: UIButton!
    @IBOutlet weak var googleSignInButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailSignInButton: UIButton!
    
    
    
    let initialPlaceholderColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.8)
    let invalidPlaceholderColor = UIColor.init(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.8)
    let textFieldValidFontColor = UIColor.white
    let textFieldInvalidFontColor = UIColor.red
    
    var isEmailAddressValid: Bool = false
    var isValidAccount: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    // TODO: Handle APPLICATION_IN_FOREGROUND
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleGoogleSignIn(_:)), name: NSNotification.Name(rawValue: Constants.GOOGLE_ACCESS_TOKEN_RECEIVED), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configureView() {
        addKeyBoardDismissOnTap()
        
        self.fbSignInButton.layer.borderColor = UIColor.white.cgColor
        self.fbSignInButton.layer.borderWidth = 1.0
        
        self.googleSignInButton.layer.borderColor = UIColor.white.cgColor
        self.googleSignInButton.layer.borderWidth = 1.0
        
        self.emailTextField.setTextFieldPlaceholder(withPlaceholderText: "Email", andPlaceholderTextColor: initialPlaceholderColor)
        self.passwordTextField.setTextFieldPlaceholder(withPlaceholderText: "Password", andPlaceholderTextColor: initialPlaceholderColor)
    }

    
    func openTermsOfUseAndPrivacyPolicy() {
        if let url = URL(string: Configuration.TERMS_PRIVACY_URL) {
            UIApplication.shared.open(url)
        }
    }
    
    // MARK: IBActions
    @IBAction func fbSignInButtonPressed(_ sender: UIButton) {
        print("SignInVC fbSignInButtonPressed")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.socialAuthType = 1
        self.handleFacebookLogin()
    }

    @IBAction func googleSignInButtonPressed(_ sender: UIButton) {
        print("SignInVC googleSignInButtonPressed")
        //self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.TEMP_FEATURE_NOT_IMPLEMENTED_MESSAGE)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.socialAuthType = 2
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func emailSignInButtonPressed(_ sender: UIButton) {
        print("SignInVC emailSignInButtonPressed")
        validateSignInCredentials()
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        performSegue(segueID: "SignUpSegue")
    }
    
    @IBAction func termsOfServiceButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func privacyPolicyButtonPressed(_ sender: UIButton) {
        openTermsOfUseAndPrivacyPolicy()
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            self.dismissViewController()
        }
        else {
            //self.viewForWebView.isHidden = true
        }
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.textColor = textFieldValidFontColor
        if textField == self.emailTextField {
            isValidAccount = false
            textField.setTextFieldPlaceholder(withPlaceholderText: "Email", andPlaceholderTextColor: initialPlaceholderColor)
        }
        else {
            textField.setTextFieldPlaceholder(withPlaceholderText: "Password", andPlaceholderTextColor: initialPlaceholderColor)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.text = textField.text!.trimmingCharacters(in: .whitespaces)
        textField.text = textField.text!.replacingOccurrences(of: " ", with: "")
        
        if textField == self.emailTextField {
            if (textField.text?.characters.count)! > 0 {
                if Utils.isValidEmailAddress(emailEntry: textField.text!) {
                    isEmailAddressValid = true
                    textField.textColor = textFieldValidFontColor
                    //self.validateUserEmail(email: textField.text!)
                }
                else {
                    isEmailAddressValid = false
                    textField.textColor = textFieldInvalidFontColor
                }
            }
            else {
                isEmailAddressValid = false
                textField.setTextFieldPlaceholder(withPlaceholderText: "Email", andPlaceholderTextColor: invalidPlaceholderColor)
            }
        }
        else {
            if (textField.text?.characters.count)! > 0 {
                textField.textColor = textFieldValidFontColor
            }
            else {
                textField.setTextFieldPlaceholder(withPlaceholderText: "Password", andPlaceholderTextColor: invalidPlaceholderColor)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.emailTextField.isFirstResponder {
            self.passwordTextField.becomeFirstResponder()
        }
        else {
            self.passwordTextField.resignFirstResponder()
            
            if (self.emailTextField.text?.characters.count)! > 0 && (self.passwordTextField.text?.characters.count)! > 0 {
                validateSignInCredentials()
            }
        }
        return true
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension SignInViewController {
    
    // NOTE: Currently not in use
    func validateUserEmail(email: String) {
        WebServiceCall.checkEmail(email: email, successBlock: { (results: Any) in
            //print("SignInViewController validateUserEmail | success results : \(results)")
            DispatchQueue.main.sync(execute: {
                if results as? String == "false" {
                    print("SignInViewController validateUserEmail | success results : false --> user with \(email) does not exist")
                    self.isValidAccount = false
                }
                else {
                    print("SignInViewController validateUserEmail | success results : true --> user with \(email) exists")
                    self.isValidAccount = true
                }
            })
        })
        { (errorCode: Int, error: String) in
            print("SignInViewController validateUserEmail | failure error : \(error)")
            self.isValidAccount = false
        }
    }

    func validateSignInCredentials() {
        if isEmailAddressValid && (self.passwordTextField.text?.characters.count)! > 0 {
            //if isValidAccount {
                let email = self.emailTextField.text!
                let passwordEncrypted = self.passwordTextField.text!.sha256()
                print("SignInVC validateSignInCredentials | email : \(email)")
                print("SignInVC validateSignInCredentials | password : \(self.passwordTextField.text!)")
                print("SignInVC validateSignInCredentials | passwordEncrypted : \(passwordEncrypted)")
                signInUser(email: email, password: passwordEncrypted)
            //}
            //else {
                //self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: "A MillionSpaces account does not exist with \(self.emailTextField.text!) email address!")
            //}
        }
        else {
            self.emailTextField.textColor = textFieldInvalidFontColor
            self.passwordTextField.textColor = textFieldInvalidFontColor
            self.emailTextField.setTextFieldPlaceholder(withPlaceholderText: "Email", andPlaceholderTextColor: invalidPlaceholderColor)
            self.passwordTextField.setTextFieldPlaceholder(withPlaceholderText: "Password", andPlaceholderTextColor: invalidPlaceholderColor)
        }
    }
    
    
    func signInUser(email: String, password: String) {
        
        self.showActivityIndicator()

        WebServiceCall.authenticateUser(username: email, password: password, successBlock: { (results: Any) in
            print("SignInVC signInUser | results : \(results)")
            DispatchQueue.main.sync(execute: {
                AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                self.dismissViewController()
                self.hideActivityIndicator()
            })
        })
        { (errorCode: Int, error: String) in
            print("SignInVC signInUser | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.hideActivityIndicator()
                if errorCode == Constants.STATUS_CODE_UNAUTHORIZED {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                }
                else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.presentServerErrorAlert()
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.presentNetworkUnavailabilityErrorAlert()
                }
                else {
                    self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
}



// Handle Facebook Login
extension SignInViewController {

    func handleFacebookLogin() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((FBSDKAccessToken.current()) != nil) {
                            print("SignInVC handleFacebookLogin | fb access token : \(FBSDKAccessToken.current().tokenString!)")
                            
                            self.showActivityIndicator()
                            
                            WebServiceCall.authenticateUser(withFacebookAccessToken: "\(FBSDKAccessToken.current().tokenString!)", successBlock: { (results: Any) in
                                
                                DispatchQueue.main.sync(execute: {
                                    AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                                    self.dismissViewController()
                                    self.hideActivityIndicator()
                                })
                            })
                            { (errorCode: Int, error: String) in
                                print("SignInVC handleFacebookLogin sign in | error")// : \(error)")
                                DispatchQueue.main.sync(execute: {
                                    self.hideActivityIndicator()
                                    
                                    if errorCode == Constants.STATUS_CODE_UNAUTHORIZED { // TODO
                                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                                    }
                                    else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                                        self.presentServerErrorAlert()
                                    }
                                    else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                                        self.presentNetworkUnavailabilityErrorAlert()
                                    }
                                    else {
                                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    }
    
}


// Handle Googlee Login
extension SignInViewController {

    func handleGoogleSignIn(_ notification: Notification) {
        
        if let accessToken = notification.userInfo?["ACCESS_TOKEN"] {
            self.showActivityIndicator()
            
            WebServiceCall.authenticateUser(withGoogleAccessToken: accessToken as! String, successBlock: { (results: Any) in
                DispatchQueue.main.sync(execute: {
                    print("SignInVC handleGoogleSignIn | results : \(results)")
                    AuthenticationManager.setAuthenticatedUser(userDetails: results as! [String : Any])
                    self.dismissViewController()
                    self.hideActivityIndicator()
                })
            })
            { (errorCode: Int, error: String) in
                print("SignInVC handleGoogleSignIn | errorCode : \(errorCode) | error : \(error)")
                DispatchQueue.main.sync(execute: {
                    self.hideActivityIndicator()
                    
                    if errorCode == Constants.STATUS_CODE_UNAUTHORIZED { // TODO
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.SIGN_IN_WRONG_CREDENTIALS_ERROR_MESSAGE)
                    }
                    else if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                        self.presentServerErrorAlert()
                    }
                    else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                        self.presentNetworkUnavailabilityErrorAlert()
                    }
                    else {
                        self.presentDismissableAlertWithMessage(title: AlertMessages.ERROR_TITLE, message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    }
                })
            }
        }
    }
}
