//
//  LogInViewController.swift
//  EventSpace
//
//  Created by Randika Swaris on 5/19/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
//import Firebase
//import GoogleSignIn
//import FirebaseAuth

class LogInViewController: BaseViewController { //, GIDSignInUIDelegate
    
    public var onSignInSuccess: (()->Void )? = nil
    public var onSignInError: (()->Void )? = nil
    
   // @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBAction func googleSignInTapped(_ sender: Any) {
        self.activityIndicator.startAnimating()
        //GIDSignIn.sharedInstance().signIn()
    }
    
    /*
    public func sign(inWillDispatch signIn: GIDSignIn!, error: Error!){
        print(signIn.currentUser.authentication.accessToken)
        logInWithGoogle(token: signIn.currentUser.authentication.accessToken)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    public func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        
    }*/
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sign In"
        //GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func logInWithGoogle(token: String){
        let url = URL(string: "TEMP")
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let paramString = "googleToken=\(token)"
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            guard let _: Data = data, let _: URLResponse = response, error == nil else {
                print("Error In signing In.........")
                DispatchQueue.main.sync(execute: {
                    self.activityIndicator.stopAnimating()
                })
                return
            }
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            //print("Google Sign In Response \(String(describing: dataString))") //JSONSerialization
            self.onSignInSuccess?()
            LogInManager.isLoggedIn = true
            DispatchQueue.main.sync(execute: {
                self.activityIndicator.stopAnimating()
                self.popViewController()
            })
        }
        task.resume()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
