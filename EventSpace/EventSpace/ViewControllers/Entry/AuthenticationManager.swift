//
//  AuthenticationManager.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 10/6/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class AuthenticationManager: NSObject {
    
    static var isUserAuthenticated: Bool = false
    static var user: User?
    
    
    static func setAuthenticatedUser(userDetails: [String : Any]) {
        let u = User()
        u.initUser(withUserDetailsDictionary: userDetails)
        self.user = u
    }
    
    
    static func setAuthenticationCookies(withAuthenticationResponse response: URLResponse) {
        if let httpResponse = response as? HTTPURLResponse {
            print("AuthenticationManager setAuthenticationCookies")
            //let authCookies = HTTPCookie.cookies(withResponseHeaderFields: httpResponse.allHeaderFields as! [String : String], for: httpResponse.url!)
            //HTTPCookieStorage.shared.setCookies(authCookies, for: httpResponse.url!, mainDocumentURL: nil)
            
            let authCookieStorageDictionary: [String: Any] = [Constants.KEY_AUTH_RESPONSE_COOKIES: HTTPCookieStorage.shared.cookies!, Constants.KEY_AUTH_RESPONSE_URL: httpResponse.url!]
            let authCookieStorageDictionaryData = NSKeyedArchiver.archivedData(withRootObject: authCookieStorageDictionary)
            
            Constants.USER_DEFAULTS.set(authCookieStorageDictionaryData, forKey: Constants.KEY_AUTH_COOKIE_STORAGE_DATA)
            Constants.USER_DEFAULTS.synchronize()
            
            self.isUserAuthenticated = true
        }
    }
    
    
    static func resetAuthenticationCookiesForAuthenticatedUser() {
        if let authCookieStorageDataDictionaryData = Constants.USER_DEFAULTS.data(forKey: Constants.KEY_AUTH_COOKIE_STORAGE_DATA) {
            print("AuthenticationManager setAuthenticationCookiesForAuthenticatedUser")
            let authCookieStorageDataDictionary = NSKeyedUnarchiver.unarchiveObject(with: authCookieStorageDataDictionaryData) as! [String: Any]
            let authCookies = authCookieStorageDataDictionary[Constants.KEY_AUTH_RESPONSE_COOKIES] as! [HTTPCookie]
            let authResponseUrl = authCookieStorageDataDictionary[Constants.KEY_AUTH_RESPONSE_URL] as! URL
            HTTPCookieStorage.shared.setCookies(authCookies, for: authResponseUrl, mainDocumentURL: nil)
            
            self.isUserAuthenticated = true
        }
        else {
            self.isUserAuthenticated = false
            self.user = nil
        }
    }
    
    
    static func logOutAuthenticatedUser() {
        let authCookieStorage = HTTPCookieStorage.shared
        for cookie in authCookieStorage.cookies ?? [] {
            authCookieStorage.deleteCookie(cookie)
        }
        
        if Constants.USER_DEFAULTS.data(forKey: Constants.KEY_AUTH_COOKIE_STORAGE_DATA) != nil {
            Constants.USER_DEFAULTS.removeObject(forKey: Constants.KEY_AUTH_COOKIE_STORAGE_DATA)
            Constants.USER_DEFAULTS.synchronize()
            
            self.isUserAuthenticated = false
            self.user = nil
            
            print("AuthenticationManager logOutAuthenticatedUser + deleteAuthenticationCookies")
        }
    }
    
    
    
    
    
    
    
    
    
    // NOT IN USE
    private static func saveAuthenticationCookies1241226363(response: URLResponse) {
        if let httpResponse = response as? HTTPURLResponse {
            print("AuthenticationManager saveAuthenticationCookies | response url     : \(response.url!)")
            print("AuthenticationManager saveAuthenticationCookies | httpResponse url : \(httpResponse.url!)")
            let cookies = HTTPCookie.cookies(withResponseHeaderFields: httpResponse.allHeaderFields as! [String : String], for: httpResponse.url!)
            HTTPCookieStorage.shared.setCookies(cookies, for: httpResponse.url!, mainDocumentURL: nil)
            
            for cookie in cookies {
                var cookieProperties = [HTTPCookiePropertyKey: Any]()
                
                cookieProperties[HTTPCookiePropertyKey.name] = cookie.name
                cookieProperties[HTTPCookiePropertyKey.value] = cookie.value
                cookieProperties[HTTPCookiePropertyKey.domain] = cookie.domain
                cookieProperties[HTTPCookiePropertyKey.path] = cookie.path
                cookieProperties[HTTPCookiePropertyKey.version] = NSNumber(value: cookie.version)
                cookieProperties[HTTPCookiePropertyKey.expires] = cookie.expiresDate
                //cookieProperties[HTTPCookiePropertyKey.discard] = cookie.isSessionOnly
                
                let newCookie = HTTPCookie.init(properties: cookieProperties) // NSHTTPCookie(properties: cookieProperties)
                HTTPCookieStorage.shared.setCookie(newCookie!)
                
                print("name: \(cookie.name) value: \(cookie.value)")
            }
        }
    }

}
