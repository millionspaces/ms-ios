//
//  LogInManager.swift
//  EventSpace
//
//  Created by Randika Swaris on 5/23/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//



import UIKit

class LogInManager: NSObject {
    
    public static var isLoggedIn: Bool = false
    
    public static func login(onSuccess: @escaping (() -> Void), onError: @escaping (() -> Void)) {
        let logInViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"LogInViewController") as! LogInViewController
        logInViewController.onSignInSuccess = onSuccess;
        logInViewController.onSignInError = onError;
        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
        navigationController.pushViewController(logInViewController, animated:true)
    }
}
