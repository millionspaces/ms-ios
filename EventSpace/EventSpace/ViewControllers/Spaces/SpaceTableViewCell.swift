//
//  SpaceTableViewCell.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/24/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import SDWebImage

class SpaceTableViewCell: UITableViewCell {
    
    static let TABLE_CELL_TAGS = (image: 101, name: 200, address_2: 201,  participantCount: 202, ratePerHour: 203, ratingView: 301, parentView: 401)
    
    static func populateCell(cell: UITableViewCell, space: Space) -> UITableViewCell {
        
        if let parentView = cell.viewWithTag(TABLE_CELL_TAGS.parentView) {
            parentView.layer.shadowColor = UIColor.black.cgColor
            parentView.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
            parentView.layer.shadowRadius = 2
            parentView.layer.shadowOpacity = 0.5
        }
        
        Utils.setLabelText(forTableViewCell: cell, withTag: TABLE_CELL_TAGS.name, andText: space.name)
        
        Utils.setLabelText(forTableViewCell: cell, withTag: TABLE_CELL_TAGS.address_2, andText: space.addressLine2 ?? space.name!)
        
        Utils.setLabelText(forTableViewCell: cell, withTag: TABLE_CELL_TAGS.participantCount, andText: String(describing: space.participantCount!))
        
        let formattedNumber = Utils.formatPriceValues(price: space.ratePerHour!)
        Utils.setLabelText(forTableViewCell: cell, withTag: TABLE_CELL_TAGS.ratePerHour, andText: "LKR \(formattedNumber)")
        
        // ImageView
        let imageView = cell.viewWithTag(TABLE_CELL_TAGS.image) as! UIImageView
        //imageView.image = nil
        //imageView.imageFromServerURL(urlString: (Configuration.PLACEHOLDER_IMAGE_BASE_URL + space.thumbnailImage))
        //imageView.imageFromServerURL(urlString: (Configuration.IMAGE_BASE_URL + space.thumbnailImage))
        //imageView.sd_setImage(with: URL(string: (Configuration.PLACEHOLDER_IMAGE_BASE_URL + space.thumbnailImage)), completed: nil)
        imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + space.thumbnailImage)), completed: nil)
        //imageView.sd_setImage(with: URL(string: (Configuration.IMAGE_BASE_URL + space.thumbnailImage)), placeholderImage: #imageLiteral(resourceName: "img-thumbnail"))
        
        //var rating = Float(space.rating)
        // TODO: Remove hard coded Value
        //rating = 2.5
        //(cell.viewWithTag(TABLE_CELL_TAGS.ratingView) as! StarRatingView).setRatingValue(withFloat: rating)
        
        return cell
    }
}
