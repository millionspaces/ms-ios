//
//  SpaceViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 6/14/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit

class SpaceViewController: BaseViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var eventSpaceTableView: UITableView!
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var filtersButton: UIButton!
    
    @IBOutlet weak var optionsView: UIView! // ---> Dimmed background view for sortOptionsView
    @IBOutlet weak var constOptionViewBottom: NSLayoutConstraint!
    @IBOutlet weak var sortOptionsView: UIView!
    @IBOutlet weak var constSortOptionsSubviewBottom: NSLayoutConstraint!
    
    // MARK: Global Constants/Variables
    let SORT_OPTIONS_VIEW_TAGS = (distance: 1, priceLowToHight: 2, priceHighToLow: 3)
    
    var selectedSortOption: String?
    
    var filteredSpaces: [Space] = []
    var totalFilteredSpacesCount: Int = 0
    var currentPage: Int = 0
    
    var searchBar: UISearchBar?
    
    var alertController: UIAlertController?
    
    override func viewWillAppear(_ animated: Bool) {
        //print("SpaceViewController viewWillAppear")
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIApplication.shared.isStatusBarHidden = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToSpacesFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
        
        self.constOptionViewBottom.constant = 0.0
        
        self.initSearchBar()
        
        if FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters {
            currentPage = 0
            self.filteredSpaces.removeAll()
            loadFilteredSpaces()
        }
        
        configureSortOptionsViewSelection()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //print("SpaceViewController viewDidLoad")
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        configureView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //print("SpaceViewController viewDidAppear")
    }
    
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }*/
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.removeObserver(self)
        
        if let search = self.searchBar {
            search.removeFromSuperview()
            self.searchBar = nil
        }
    }
    
    
    func returnedToSpacesFromBackground() {
        if Utils.didPassDuration(durationInHours: 0.5) { // 0.003 ~ 10s
            alertController?.dismiss(animated: false, completion: nil)
            self.baseCommonAlertController?.dismiss(animated: false, completion: nil)
            self.popToRootViewController()
        }
    }

    func configureView() {
        self.navigationItem.title = "MillionSpaces"
        
        self.sortOptionsView.isHidden = true
        self.constSortOptionsSubviewBottom.constant = -160.0
    }
    
    
    func loadFilteredSpaces() {
        print("SpaceViewController loadFilteredSpaces | filters: \(FilterManager.retrieveFiltersForWebRequest())")
        self.showActivityIndicator()
    
        WebServiceCall.retrieveSpaces(withPageNumber: currentPage, successBlock: { (results: Any) in
            //print("SpaceViewController loadFilteredSpaces | success results : \(results)")
            
            if let _results = results as? [String : Any] {
                
                self.totalFilteredSpacesCount = _results["count"] as! Int
                
                let arrSpaceResults = _results["spaces"] as! [NSDictionary]
                
                for dic: NSDictionary in arrSpaceResults {
                    let space = Space()
                    space.initSpace(withDictionary: dic as! [String : Any])
                    self.filteredSpaces.append(space)
                }
                
                print("SpaceViewController loadFilteredSpaces | success totalFilteredSpacesCount : \(self.totalFilteredSpacesCount)")
                print("SpaceViewController loadFilteredSpaces | success arrSpaceResults count : \(arrSpaceResults.count)")
                print("SpaceViewController loadFilteredSpaces | success filteredSpaces count : \(self.filteredSpaces.count)")
        
                
                DispatchQueue.main.sync(execute: {
                    //self.testSpaces()
                    
                    let isCurrentPage_0 = (self.currentPage == 0) ? true : false
                    
                    if self.totalFilteredSpacesCount != self.filteredSpaces.count {
                        self.currentPage += 1
                    }
                    
                    self.eventSpaceTableView.reloadData()
                    
                    if isCurrentPage_0 && self.totalFilteredSpacesCount > 0 {
                        self.eventSpaceTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
                    }
                    
                    if self.totalFilteredSpacesCount > 0 {
                        self.navigationItem.title = self.totalFilteredSpacesCount == 1 ? "1 Space" : "\(String(self.totalFilteredSpacesCount)) Spaces"
                        self.eventSpaceTableView.isHidden = false
                        self.sortButton.isUserInteractionEnabled = true
                    }
                    else {
                        self.navigationItem.title = "MillionSpaces"
                        self.eventSpaceTableView.isHidden = true
                        self.sortButton.isUserInteractionEnabled = false
                    }
                    
                    self.hideActivityIndicator()
                })
            }
            else {
                DispatchQueue.main.sync(execute: {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                    self.eventSpaceTableView.reloadData()
                    self.hideActivityIndicator()
                })
            }
        })
        { (errorCode: Int, error: String) in
            print("SpaceViewController loadFilteredSpaces | failure | errorCode : \(errorCode) | error : \(error)")
            DispatchQueue.main.sync(execute: {
                self.eventSpaceTableView.reloadData()
                self.hideActivityIndicator()
                if Constants.STATUS_CODE_SERVER_ERROR_RANGE.contains(errorCode) {
                    self.addSpacesReloadAlert(message: AlertMessages.SERVER_ERROR_MESSAGE)
                }
                else if errorCode == Constants.ERROR_CODE_INTERNET_OFFLINE || errorCode == Constants.ERROR_CODE_NETWORK_CONNECTION_LOST {
                    self.addSpacesReloadAlert(message: AlertMessages.INTERNET_OFFLINE_MESSAGE)
                }
                else {
                    self.addSpacesReloadAlert(message: AlertMessages.UNKNOWN_ERROR_MESSAGE)
                }
            })
        }
    }
    
    func testSpaces() {
        print("")
        for space in self.filteredSpaces {
            print("SpaceVC testSpaces | space --> name : \(space.name!) | eventTypes count : \(space.eventTypes.count) | seatingArrangements count : \(space.seatingArrangements.count)")
        }
        print("")
    }
    
    func addSpacesReloadAlert(message: String) {
        alertController = UIAlertController(title: AlertMessages.ERROR_TITLE, message: message, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: AlertMessages.RELOAD_ACTION, style: .default) { (alert: UIAlertAction) in
            self.loadFilteredSpaces()
        }
        let dismissAction = UIAlertAction(title: AlertMessages.DISMISS_ACTION, style: .default, handler: nil)
        alertController!.addAction(dismissAction)
        alertController!.addAction(reloadAction)
        self.present(alertController!, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if self.sortOptionsView.frame.contains(location) {
            hideSortOptionsView()
        }
    }
    
    func initSearchBar() {
        self.searchBar = UISearchBar()
        self.searchBar?.frame = CGRect.init(x: 10.0, y: 0.0, width: Utils.screenWidth - 10.0, height: 44.0)
        self.searchBar?.searchBarStyle = UISearchBarStyle.default
        self.searchBar?.backgroundColor = ColorUtils.MS_DARK_BLUE_COLOR
        self.searchBar?.tintColor = UIColor.lightGray
        //UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar!)]).tintColor = .black
        self.searchBar?.keyboardAppearance = .dark
        self.searchBar?.showsCancelButton = true
        self.searchBar?.placeholder = "Search"
        self.searchBar?.enablesReturnKeyAutomatically = false
        self.searchBar?.delegate = self
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.popViewController()
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if let search = self.searchBar {
            self.navigationController?.navigationBar.addSubview(search)
            search.becomeFirstResponder()
        }
    }
    
    @IBAction func filtersButtonPressed(_ sender: UIButton) {
        performSegue(segueID: "FiltersSegue")
    }
    
    @IBAction func sortButtonPressed(_ sender: UIButton) {
        showSortOptionsView()
    }
    
    @IBAction func sortOptionButtonPressed(_ sender: UIButton) {
        if sender.tag == SORT_OPTIONS_VIEW_TAGS.distance {
            if FilterManager.spaceFilter.location != nil {
                FilterManager.spaceFilter.sortBy = nil
            }
            else {
                self.presentDismissableAlertWithMessage(title: AlertMessages.ALERT_TITLE, message: AlertMessages.LOCATION_SORT_WARNING_MESSAGE)
                //hideSortOptionsView()
                return
            }
        }
        else if sender.tag == SORT_OPTIONS_VIEW_TAGS.priceLowToHight {
            FilterManager.spaceFilter.sortBy = StaticData.FILTER_SORT_OPTIONS.PLf
        }
        else if sender.tag == SORT_OPTIONS_VIEW_TAGS.priceHighToLow {
            FilterManager.spaceFilter.sortBy = StaticData.FILTER_SORT_OPTIONS.PHf
        }
        
        configureSortOptionsViewSelection()
        hideSortOptionsView()
        
        if FilterManager.spaceFilter.location != nil || FilterManager.spaceFilter.sortBy != nil {
            currentPage = 0
            self.filteredSpaces.removeAll()
            loadFilteredSpaces()
        }
    }
    

    func configureSortOptionsViewSelection() {
        if let sortBy = FilterManager.spaceFilter.sortBy {
            if sortBy == StaticData.FILTER_SORT_OPTIONS.PLf {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceLowToHight) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.distance) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceHighToLow) as! UIButton).isSelected = false
            }
            else {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceHighToLow) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.distance) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceLowToHight) as! UIButton).isSelected = false
            }
        }
        else {
            if FilterManager.spaceFilter.location != nil {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.distance) as! UIButton).isSelected = true
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceLowToHight) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceHighToLow) as! UIButton).isSelected = false
            }
            else {
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.distance) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceLowToHight) as! UIButton).isSelected = false
                (self.sortOptionsView.viewWithTag(SORT_OPTIONS_VIEW_TAGS.priceHighToLow) as! UIButton).isSelected = false
            }
        }
    }
    
    
    func showSortOptionsView() {
        self.sortOptionsView.isHidden = false
        self.constSortOptionsSubviewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSortOptionsView() {
        self.constSortOptionsSubviewBottom.constant = -160.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        perform(#selector(hideSortOptionsViewDelayedFunction), with: nil, afterDelay: 0.5)
    }
    
    func hideSortOptionsViewDelayedFunction() {
        self.sortOptionsView.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}


// MARK: - UIScrollViewDelegate
extension SpaceViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        if let search = self.searchBar, search.isFirstResponder {
            self.searchBar!.endEditing(true)
            self.searchBar!.removeFromSuperview()
        }
        
        self.constOptionViewBottom.constant = -50.0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.constOptionViewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.7, options: UIViewAnimationOptions.allowUserInteraction, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension SpaceViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.runSearch(keyword: searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar!.endEditing(true)
        self.searchBar!.removeFromSuperview()
    }
    
    func runSearch(keyword: String?) {
        self.searchBar!.endEditing(true)
        self.searchBar!.removeFromSuperview()
        FilterManager.spaceFilter.searchKeyword = (keyword != nil) ? "\(keyword!)" : nil
        currentPage = 0
        self.filteredSpaces.removeAll()
        loadFilteredSpaces()
    }
}


// MARK: - UITableViewDataSource
extension SpaceViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredSpaces.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpaceCell", for: indexPath) as! SpaceTableViewCell
        let spaceAtIndex  = (self.filteredSpaces)[indexPath.row]
        return SpaceTableViewCell.populateCell(cell: cell, space:spaceAtIndex)
    }
    
}

// MARK: - UITableViewDelegate
extension SpaceViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let spaceAtIndex = self.filteredSpaces[indexPath.row]
        FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = false
        let spaceDetailsController = self.storyboard?.instantiateViewController(withIdentifier: "SpaceDetailsVC") as! SpaceDetailsViewController
        spaceDetailsController.displayingSpaceId = spaceAtIndex.id
        print("SpaceVC didSelectRowAt spaceAtIndex id : \(spaceAtIndex.id!)")
        self.navigationController?.pushViewController(spaceDetailsController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.filteredSpaces.count - 1) && (self.totalFilteredSpacesCount != self.filteredSpaces.count) {
            loadFilteredSpaces()
        }
    }
}
