//
//  FilterViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/24/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import GooglePlaces

class FilterViewController: BaseViewController {
    
    @IBOutlet weak var filtersTableView: UITableView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var viewForDateTimePickers: UIView!
    @IBOutlet weak var pickersViewCancelButton: UIButton!
    @IBOutlet weak var pickersViewSetButton: UIButton!
    @IBOutlet weak var constViewForDateTimePickersSubviewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var hourPicker: UIPickerView!
    @IBOutlet weak var amPmPicker: UIPickerView!
    
    // MARK: Displaying Meta Data / Static Data
    var eventTypes = [EventType]()
    var amenities = [Amenity]()
    var rules = [Rule]()
    var seatingArrangements = [SeatingArrangement]()
    var spaceCapacityOptions = [[String : Any]]()
    var priceOptions = [[String : Any]]()
    
    var expandedSections: NSMutableArray = []


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadFilterData()
        self.configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToFiltersFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func returnedToFiltersFromBackground() {
        if Utils.didPassDuration(durationInHours: 1.0) {  // ~ 10s
            FilterManager.resetFilters()
            self.dismissViewController()
        }
    }
    
    
    
    func loadFilterData() {
        eventTypes = MetaData.retrieveEventTypes() as! [EventType]
        amenities = MetaData.retrieveAmenities() as! [Amenity]
        rules = MetaData.retrieveSpaceRules() as! [Rule]
        seatingArrangements = MetaData.retrieveSeatingArrangements() as! [SeatingArrangement]
        spaceCapacityOptions = StaticData.FILTER_CAPACITY_OPTIONS
        priceOptions = StaticData.FILTER_PRICE_OPTIONS
        
        print("FilterVC loadMetaData | eventTypes count : \(eventTypes.count)")
        print("FilterVC loadMetaData | amenities count : \(amenities.count)")
        print("FilterVC loadMetaData | rules count : \(rules.count)")
        print("FilterVC loadMetaData | seatingArrangements count : \(seatingArrangements.count)")
        print("FilterVC loadMetaData | spaceCapacityOptions count : \(spaceCapacityOptions.count)")
        print("FilterVC loadMetaData | priceOptions count : \(priceOptions.count)")
    }
    
    func configureView() {
        self.navigationItem.title = "Filter Categories"
        
        self.filtersTableView.allowsMultipleSelection = true
        
        self.viewForDateTimePickers.isHidden = true
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        
        self.pickersViewCancelButton.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
        self.pickersViewCancelButton.layer.borderWidth = 1.0
        self.pickersViewCancelButton.layer.cornerRadius = 10.0
        self.pickersViewCancelButton.layer.masksToBounds = true
        
        self.pickersViewSetButton.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
        self.pickersViewSetButton.layer.borderWidth = 1.0
        self.pickersViewSetButton.layer.cornerRadius = 10.0
        self.pickersViewSetButton.layer.masksToBounds = true
        
        self.datePicker.minimumDate = Date()
        self.hourPicker.delegate = self
        self.amPmPicker.delegate = self
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if self.viewForDateTimePickers.frame.contains(location) && !self.viewForDateTimePickers.viewWithTag(3)!.frame.contains(location) {
            hideViewForDateTimePickers()
        }
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = false
        dismissViewController()
    }
    
    @IBAction func resetButtonPressed(_ sender: UIButton) {
        FilterManager.resetFilters()
        dismissViewController()
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        FilterManager.spaceFilter.shouldRefreshSpacesListWithFilters = true
        dismissViewController()
    }
    
    @IBAction func pickersViewCancelButtonPressed(_ sender: UIButton) {
        hideViewForDateTimePickers()
    }
    
    @IBAction func pickersViewSetButtonPressed(_ sender: UIButton) {
        let setDate = self.datePicker.date
        print("FilterVC pickersViewSetButtonPressed | setDate : \(setDate)")
        let setTimeHour = self.hourPicker.selectedRow(inComponent: 0) + 1
        let setAmPm = self.amPmPicker.selectedRow(inComponent: 0)
        FilterManager.setAvailableDateFilter(date: setDate, hour: setTimeHour, amPm: setAmPm)
        hideViewForDateTimePickers()
        self.filtersTableView.reloadData()
    }
    
    func dateTimeCellButtonPressed(_ sender: UIButton) {
        print("FilterVC dateTimeCellButtonPressed")
        showViewForDateTimePickers()
        
    
    }
    
    func locationCellButtonPressed(_ sender: UIButton) {
        print("FilterVC locationCellButtonPressed")
        let autocompleteController = CustomGoogleAutocompleteViewController()
        autocompleteController.delegate = self
        let autocompleteFilter = GMSAutocompleteFilter()
        autocompleteFilter.country = "LK"
        //autocompleteFilter.type = GMSPlacesAutocompleteTypeFilter.region
        autocompleteController.autocompleteFilter = autocompleteFilter
        present(autocompleteController, animated: true, completion: nil)
        //isLocationSelectorOpen = true
    }
    
    func showViewForDateTimePickers() {
        self.viewForDateTimePickers.isHidden = false
        self.constViewForDateTimePickersSubviewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideViewForDateTimePickers() {
        self.constViewForDateTimePickersSubviewBottom.constant = -250.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        perform(#selector(hidePickersView), with: nil, afterDelay: 0.5)
    }
    
    func hidePickersView() {
        self.viewForDateTimePickers.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


// MARK: - UIPickerView DataSource and Delegate
extension FilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY.count
        }
        return StaticData.AMPM_ARRAY.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == hourPicker {
            return StaticData.HOUR_ARRAY[row]
        }
        return StaticData.AMPM_ARRAY[row]
    }    
}

// MARK: - Configure Table View
extension FilterViewController {
    
    func configureSectionHeader(headerView: UIView, section: Int) -> UIView {
        
        let headerLabel = headerView.viewWithTag(2) as! UILabel
        
        if section == 0 {
            headerLabel.text = "Event Type"
        }
        else if section == 1 {
            headerLabel.text = "Space Type"
        }
        else if section == 2 {
            headerLabel.text = "Location"
        }
        else if section == 3 {
            headerLabel.text = "Date & Time"
        }
        else if section == 4 {
            headerLabel.text = "Space Capacity"
        }
        else if section == 5 {
            headerLabel.text = "Starting Price"
        }
        else if section == 6 {
            headerLabel.text = "Amenities"
        }
        else if section == 7 {
            headerLabel.text = "Space Rules"
        }
        else if section == 8 {
            headerLabel.text = "Seating Arrangements"
        }
        
        headerView.tag = section
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(handleSectionSelection(_:)))
        headerView.addGestureRecognizer(tap)
        
        return headerView
    }
    
    
    func calculateNumberOfRowsForSection(section: Int) -> Int {
        
        var rowsCount: Int!
        
        if section == 0 { // Event Types
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 1 { // Space Type
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 2 { // Location
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 3 { // Date & Time
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 4 { // Number of guests
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 5 { // Rate per hour
            rowsCount = expandedSections.contains(section) ? priceOptions.count : 0
        }
        else if section == 6 { // Amenities
            rowsCount = expandedSections.contains(section) ? 1 : 0
        }
        else if section == 7 { // Rules
            rowsCount = expandedSections.contains(section) ? rules.count : 0
        }
        else if section == 8 { // Seating Arrangements
            rowsCount = expandedSections.contains(section) ? seatingArrangements.count : 0
        }
        
        return rowsCount
    }
    
    func calculateHeightForRowInSection(section: Int) -> CGFloat {
        
        if section == 0 { // Event Types Collecion View Embedded Cell
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: eventTypes.count, cvCellHeight: 40, itemsPerRow: 2)
        }
        else if section == 1 { // Space Type (for 3 CV cells)
            return 80
        }
        else if section == 2 || section == 3 { // Location or Date/Time
            return 60
        }
        else if section == 4 { // Guest Count Options Collecion View Embedded Cell
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: spaceCapacityOptions.count, cvCellHeight: 40, itemsPerRow: 2)
        }
        else if section == 6 { // Amenities Collecion View Embedded Cell
            return Utils.calculateVerticalCollectionViewEmbeddedContentHeight(dataCount: amenities.count, cvCellHeight: 40, itemsPerRow: 2)
        }
        else if section == 8 {
            return 50
        }
        else {
            return 40
        }
    }
}


// MARK: - Handle Section Header and Cell selection
extension FilterViewController {
    
    func handleSectionSelection(_ tap: UIGestureRecognizer){
        print("FilterViewController handleSectionSelection | tapped header tag : \(String(describing: tap.view!.tag))")
        let section = tap.view!.tag
        if expandedSections.contains(section) {
            expandedSections.remove(section)
            self.filtersTableView.reloadData()
        }
        else {
            expandedSections.add(section)
            self.filtersTableView.reloadData()
            self.filtersTableView.scrollToRow(at: IndexPath.init(row: 0, section: section), at: UITableViewScrollPosition.top, animated: true)
        }
    }
}


// MARK: - UITableViewDataSource

extension FilterViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "FilterHeaderCell")
        return configureSectionHeader(headerView: header!, section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calculateNumberOfRowsForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return calculateHeightForRowInSection(section: indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
        }
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
        }
        else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)
            
            if let filterLocation =  FilterManager.spaceFilter.location {
                (cell.viewWithTag(1) as! UITextField).text = filterLocation.name
            }
            
            (cell.viewWithTag(2) as! UIButton).addTarget(self, action: #selector(locationCellButtonPressed(_:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DatePickerCell", for: indexPath)
            (cell.viewWithTag(1) as! UITextField).text = FilterManager.getAvailableDateFilterDisplayValue()
            (cell.viewWithTag(2) as! UIButton).addTarget(self, action: #selector(dateTimeCellButtonPressed(_:)), for: .touchUpInside)
            return cell
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
        }
        else if indexPath.section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleCheckBoxCell", for: indexPath)
            let priceOptionAtRow = priceOptions[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = priceOptionAtRow["displayValue"] as? String
            if FilterManager.spaceFilter.price.contains(priceOptionAtRow["id"] as! Int) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
        }
        else if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionViewEmbeddedCell", for: indexPath) as! CollectionViewEmbeddedTableViewCell
            cell.setCollectionViewDatasourceDelegate(dataSourceDelegate: self, identifier: indexPath.section)
            return cell
        }
        else if indexPath.section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleCheckBoxCell", for: indexPath)
            let ruleAtRow = rules[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = ruleAtRow.displayName!
            if FilterManager.spaceFilter.rules.contains(ruleAtRow) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            return cell
        }
        else if indexPath.section == 8 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SingleCheckBoxCell-Other", for: indexPath)
            (cell.viewWithTag(3) as! UIImageView).image = #imageLiteral(resourceName: "img-thumbnail-small")
            let seatingAtRow = seatingArrangements[indexPath.row]
            (cell.viewWithTag(2) as! UILabel).text = seatingAtRow.name
            
            // ImageView
            let imageView = cell.viewWithTag(3) as! UIImageView
            imageView.sd_setImage(with: URL(string: seatingAtRow.iconUrl!), placeholderImage: #imageLiteral(resourceName: "img-thumbnail-small"))
            
            if FilterManager.spaceFilter.seatingArrangements.contains(seatingAtRow) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                tableView.deselectRow(at: indexPath, animated: true)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}


// MARK: - UITableViewDelegate

extension FilterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("FilterViewController didSelectRow at Section : \(indexPath.section) | Row : \(indexPath.row)")
        
        if indexPath.section == 5 { // Price
            if !FilterManager.spaceFilter.price.contains(indexPath.row) {
                FilterManager.spaceFilter.price.append(indexPath.row)
                let selectedPriceOptionCell = tableView.cellForRow(at: indexPath)
                (selectedPriceOptionCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if indexPath.section == 7 { // Rules
            let ruleAtRow = rules[indexPath.row]
            if !FilterManager.spaceFilter.rules.contains(ruleAtRow) {
                FilterManager.spaceFilter.rules.append(ruleAtRow)
                let selectedRuleCell = tableView.cellForRow(at: indexPath)
                (selectedRuleCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if indexPath.section == 8 { // Seating
            let seatingAtRow = seatingArrangements[indexPath.row]
            if !FilterManager.spaceFilter.seatingArrangements.contains(seatingAtRow) {
                FilterManager.spaceFilter.seatingArrangements.append(seatingAtRow)
                let selectedSeatingCell = tableView.cellForRow(at: indexPath)
                (selectedSeatingCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
    }

    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("FilterViewController didDeselectRowAt at Section : \(indexPath.section) | Row : \(indexPath.row)")
        
        if indexPath.section == 5 { // Price
            if let index = FilterManager.spaceFilter.price.index(of: indexPath.row) {
                FilterManager.spaceFilter.price.remove(at: index)
                let deselectedPriceOptionCell = tableView.cellForRow(at: indexPath)
                (deselectedPriceOptionCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if indexPath.section == 7 { // Rules
            let ruleAtRow = rules[indexPath.row]
            if let index = FilterManager.spaceFilter.rules.index(of: ruleAtRow) {
                FilterManager.spaceFilter.rules.remove(at: index)
                let deselectedRuleCell = tableView.cellForRow(at: indexPath)
                (deselectedRuleCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if indexPath.section == 8 { // Seating
            let seatingAtRow = seatingArrangements[indexPath.row]
            if let index = FilterManager.spaceFilter.seatingArrangements.index(of: seatingAtRow) {
                FilterManager.spaceFilter.seatingArrangements.remove(at: index)
                let deselectedSeatingCell = tableView.cellForRow(at: indexPath)
                (deselectedSeatingCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        
    }
}


extension FilterViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 0 { // et
            return eventTypes.count
        }
        else if collectionView.tag == 1 { // space type
            return StaticData.SPACE_TYPES.count
        }
        else if collectionView.tag == 4 { // capacity
            return spaceCapacityOptions.count
        }
        else if collectionView.tag == 6 { // amenities
            return amenities.count
        }
        
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        
        if collectionView.tag == 0 { // Event Type
            
            let eventTypeAtItem = eventTypes[indexPath.item]
            (cell.viewWithTag(2) as! UILabel).text = eventTypeAtItem.name
            
            if FilterManager.spaceFilter.eventTypes.contains(eventTypeAtItem) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
        else if collectionView.tag == 1 { // Space Type
            
            (cell.viewWithTag(2) as! UILabel).text = StaticData.SPACE_TYPES[indexPath.item + 1]
            
            if FilterManager.spaceFilter.spaceType.contains(indexPath.item + 1) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
        else if collectionView.tag == 4 { // Guest Count
            
            let guestCountAtItem = spaceCapacityOptions[indexPath.item]
            (cell.viewWithTag(2) as! UILabel).text = guestCountAtItem["displayValue"] as? String
            
            if FilterManager.spaceFilter.spaceCapacity.contains(indexPath.item) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
        else if collectionView.tag == 6 { // Amenities
            
            let amenityAtItem = amenities[indexPath.item]
            (cell.viewWithTag(2) as! UILabel).text = amenityAtItem.name
            
            if FilterManager.spaceFilter.amenities.contains(amenityAtItem) {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            else {
                (cell.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
                collectionView.deselectItem(at: indexPath, animated: true)
            }
        }
    
        return cell
    }
}


extension FilterViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (UIScreen.main.bounds.width - 20) / 2
        
        return CGSize(width: cellWidth, height: 40)
    }

}

extension FilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("FilterViewController collectionView didSelectItemAt : \(indexPath.item)")
        
        if collectionView.tag == 0 { // EventTypes
            let eventTypeAtItem = eventTypes[indexPath.item]
            if !FilterManager.spaceFilter.eventTypes.contains(eventTypeAtItem) {
                FilterManager.spaceFilter.eventTypes.append(eventTypeAtItem)
                let selectedEventTypeCell = collectionView.cellForItem(at: indexPath)
                (selectedEventTypeCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == 1 { // Space Type
            if !FilterManager.spaceFilter.spaceType.contains(indexPath.item + 1) {
                FilterManager.spaceFilter.spaceType.append(indexPath.item + 1)
                let selectedSpaceTypeCell = collectionView.cellForItem(at: indexPath)
                (selectedSpaceTypeCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == 4 { // Space Capacity
            if !FilterManager.spaceFilter.spaceCapacity.contains(indexPath.item) {
                FilterManager.spaceFilter.spaceCapacity.append(indexPath.item)
                let selectedSpaceCapacityCell = collectionView.cellForItem(at: indexPath)
                (selectedSpaceCapacityCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
        else if collectionView.tag == 6 { // Amenities
            let amenityAtItem = amenities[indexPath.item]
            if !FilterManager.spaceFilter.amenities.contains(amenityAtItem) {
                FilterManager.spaceFilter.amenities.append(amenityAtItem)
                let selectedAmenityCell = collectionView.cellForItem(at: indexPath)
                (selectedAmenityCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-filled")
            }
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("FilterViewController collectionView didDeselectItemAt : \(indexPath.item)")
        
        if collectionView.tag == 0 { // EventTypes
            let eventTypeAtItem = eventTypes[indexPath.item]
            if let index = FilterManager.spaceFilter.eventTypes.index(of: eventTypeAtItem) {
                FilterManager.spaceFilter.eventTypes.remove(at: index)
                let deselectedEventTypeCell = collectionView.cellForItem(at: indexPath)
                (deselectedEventTypeCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == 1 { // Space Type
            if let index = FilterManager.spaceFilter.spaceType.index(of: indexPath.item + 1) {
                FilterManager.spaceFilter.spaceType.remove(at: index)
                let deselectedSpaceTypeCell = collectionView.cellForItem(at: indexPath)
                (deselectedSpaceTypeCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == 4 { // Space Capacity
            if let index = FilterManager.spaceFilter.spaceCapacity.index(of: indexPath.item) {
                FilterManager.spaceFilter.spaceCapacity.remove(at: index)
                let deselectedSpaceCapacityCell = collectionView.cellForItem(at: indexPath)
                (deselectedSpaceCapacityCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
        else if collectionView.tag == 6 { // Amenities
            let amenityAtItem = amenities[indexPath.item]
            if let index = FilterManager.spaceFilter.amenities.index(of: amenityAtItem) {
                FilterManager.spaceFilter.amenities.remove(at: index)
                let deselectedAmenityCell = collectionView.cellForItem(at: indexPath)
                (deselectedAmenityCell!.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "ico-checkbox-empty")
            }
        }
    }
}


extension FilterViewController : GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        FilterManager.spaceFilter.location = place
        self.filtersTableView.reloadData()
        //selectedLocation = place
        print("Place name: \(place.name)")
        print("Place address: \(String(describing: place.formattedAddress))")
        print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
