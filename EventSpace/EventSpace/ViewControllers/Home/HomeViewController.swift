//
//  HomeViewController.swift
//  EventSpace
//
//  Created by Ishan Sirimanne on 7/18/17.
//  Copyright © 2017 Auxenta. All rights reserved.
//

import UIKit
import GooglePlaces

class HomeViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var dimmedBackgroundLayerView: UIView!
    @IBOutlet weak var eventTypeTableView: UITableView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var constMenuTableViewLeading: NSLayoutConstraint!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var leftSwipeForMenu: UISwipeGestureRecognizer!
    var rightSwipeForMenu: UISwipeGestureRecognizer!
    
    var isMenuVisible: Bool!
    var menuOptions: NSMutableArray = []
    var eventTypes = [EventType]()
    var selectedEventType: EventType?
    var selectedLocation: GMSPlace?
    
    var isLocationSelectorOpen: Bool = false
    
    var searchHeaderHeight: Int!
    
    
    let HOME_TABLE_VIEW_CELLS = (meetings: 0, trainingsAndPhotoshoots: 1, weddings: 2, interviews: 3)
    let HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS = (tImage: 1, tLabel: 2, tLabelBackground: 3)
    let HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS = (tImageOne: 1, tLabelOne: 2, tLabelOneBackground: 3, tImageTwo: 4, tLabelTwo: 5, tLabelTwoBackground: 6)

    let EVENT_TYPE_FILTER_CELL_TAGS = (imageView: 1, label: 2)
    
    
    
    override func viewDidLoad() {
        print("HomeViewController viewDidLoad")
        self.homeTableView.delaysContentTouches = true
        self.homeTableView.canCancelContentTouches = true
        super.viewDidLoad()
        
        self.navigationItem.title = "MillionSpaces"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.returnedToHomeFromBackground), name: NSNotification.Name(rawValue: Constants.APPLICATION_IN_FOREGROUND), object: nil)
    
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if !isLocationSelectorOpen {
            configureView()
        }
        else {
            homeTableView.reloadData()
            addMenuSwipeGestures()
            isLocationSelectorOpen = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        //self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.dimmedBackgroundLayerView.isHidden = true
        handleEventTypeTableViewVisibility(isHidden: true)
        removeMenuSwipeGestures()
        handleMenuViewVisibility(isVisible: false)
    }
    
    func returnedToHomeFromBackground() {
        configureView()
    }
    
    
    // MARK: - The following is needed for the swipe from Left gesture to go to previous screen in Navigaion Controller stack
    /*
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }*/
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer {
            return false
        }
        return true
    }
    
    private func configureView() {
        self.dimmedBackgroundLayerView.isHidden = true
        self.eventTypeTableView.isHidden = true
        self.constMenuTableViewLeading.constant = -(UIScreen.main.bounds.width)
        isMenuVisible = false
        searchHeaderHeight = 168
        
        self.homeTableView.isExclusiveTouch = false
        
        FilterManager.resetFilters()
        
        selectedEventType = nil
        selectedLocation = nil
        
        isLocationSelectorOpen = false
        
        eventTypes = MetaData.retrieveEventTypes() as! [EventType]

        configureHomeMenu()

        self.homeTableView.reloadData()
        self.homeTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
        self.eventTypeTableView.reloadData()
        
        self.appVersionLabel.text = StaticData.APP_VERSION
    }
    
    
    func tableViewHeaderButtonPressed(_ button: UIButton) {
        if button.tag == 1 {
            print("TitleView Pressed --> Hiding search fields")
            searchHeaderHeight = 168
            self.homeTableView.reloadData()
        }
        else if button.tag == 2 {
            print("Hiding View Pressed --> Search fields are shown")
            searchHeaderHeight = 268
            self.homeTableView.reloadData()
        }
        else if button.tag == 3 {
            print("Search button Pressed --> Search function begins --> Directs to spaces list")
            
            if let et = selectedEventType {
                FilterManager.spaceFilter.eventTypes.append(et)
            }
            
            if let location = selectedLocation {
                FilterManager.spaceFilter.location = location
            }
            
            performSegue(segueID: "SpacesSegue")
        }
        else if button.tag == 1111 {
            print("EventType TextField Pressed --> EventTypeTableView shown")
            handleEventTypeTableViewVisibility(isHidden: false)
        }
        else if button.tag == 2222 {
            print("Location TextField Pressed --> Location search via Google")
            let autocompleteController = CustomGoogleAutocompleteViewController()
            autocompleteController.delegate = self
            let autocompleteFilter = GMSAutocompleteFilter()
            autocompleteFilter.country = "LK"
            //autocompleteFilter.type = GMSPlacesAutocompleteTypeFilter.region
            autocompleteController.autocompleteFilter = autocompleteFilter
            present(autocompleteController, animated: true, completion: nil)
            isLocationSelectorOpen = true
        }
    }
    
    func tapHandlerForEventType(_ tap: UITapGestureRecognizer) {
        print("HomeVC tapHandler | tapped view tag : \((tap.view?.tag)!)")
        if tap.view?.tag == 7 {
            print("HomeVC tapHandler | tapped Party")
            FilterManager.spaceFilter.eventTypes.append(MetaData.retrieveEventType(with: 1))
        }
        else if tap.view?.tag == 8 {
            print("HomeVC tapHandler | tapped Photoshoot")
            FilterManager.spaceFilter.eventTypes.append(MetaData.retrieveEventType(with: 7))
        }
        
        performSegue(segueID: "SpacesSegue")
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.view) else { return }
        if dimmedBackgroundLayerView.frame.contains(location) {
            if isMenuVisible {
                handleMenuViewVisibility(isVisible: false)
            }
            else {
                handleEventTypeTableViewVisibility(isHidden: true)
            }
        }
    }
    
    func handleEventTypeTableViewVisibility(isHidden: Bool) {
        if isHidden {
            addMenuSwipeGestures()
            self.homeTableView.reloadData()
        }
        else {
            removeMenuSwipeGestures()
        }
        
        UIView.transition(with: self.dimmedBackgroundLayerView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.dimmedBackgroundLayerView.isHidden = isHidden
            self.eventTypeTableView.isHidden = isHidden
        }, completion: nil)
    }
    
    @IBAction func leftNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        // Menu Button Pressed
        self.eventTypeTableView.isHidden = true
        if isMenuVisible {
            handleMenuViewVisibility(isVisible: false)
        }
        else {
            handleMenuViewVisibility(isVisible: true)
        }
        
    }
    
    @IBAction func rightNavBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openNotificationViewController()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Handle Home Menu
extension HomeViewController {
    
    func configureHomeMenu() {
        
        // Adding Swipe Gesture to Menu
        leftSwipeForMenu = UISwipeGestureRecognizer.init(target: self, action: #selector(handleSwipeGesture(_:)))
        rightSwipeForMenu = UISwipeGestureRecognizer.init(target: self, action: #selector(handleSwipeGesture(_:)))
        
        leftSwipeForMenu.direction = .left
        rightSwipeForMenu.direction = .right
        
        addMenuSwipeGestures()
        
        
        // TODO: Check User Profile to set menu options dynamically
        
        // TEMP:
        menuOptions.removeAllObjects()
        
        if !AuthenticationManager.isUserAuthenticated {
            self.userProfileImageView.image = #imageLiteral(resourceName: "img-app-logo-small-tb")
            self.userNameLabel.text = "Sign In / Sign Up"
        }
        else {
            self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.bounds.width/2
            self.userProfileImageView.layer.masksToBounds = true
            
            if let user = AuthenticationManager.user {
                self.userNameLabel.text = user.name
                if let imgUrl = user.profileImageUrl {
                    self.userProfileImageView.sd_setImage(with: imgUrl, placeholderImage: #imageLiteral(resourceName: "img-guest-default"))
                }
                else {
                    self.userProfileImageView.image = #imageLiteral(resourceName: "img-guest-default")
                }
                
                menuOptions.addObjects(from: StaticData.HOME_MENU_OPTIONS_ARRAY as! [Any])
            }
        }
        
        self.menuTableView.reloadData()
    }
    
    func addMenuSwipeGestures() {
        self.view.addGestureRecognizer(leftSwipeForMenu)
        self.view.addGestureRecognizer(rightSwipeForMenu)
    }
    
    func removeMenuSwipeGestures() {
        self.view.removeGestureRecognizer(leftSwipeForMenu)
        self.view.removeGestureRecognizer(rightSwipeForMenu)
    }
    
    func handleSwipeGesture(_ swipe: UISwipeGestureRecognizer) {
        if swipe.direction == .left {
            handleMenuViewVisibility(isVisible: false)
        }
        else if swipe.direction == .right {
            handleMenuViewVisibility(isVisible: true)
        }
    }

    func handleMenuViewVisibility(isVisible: Bool) {
        isMenuVisible = isVisible
        
        if isVisible {
            self.constMenuTableViewLeading.constant = 0
        }
        else {
            self.constMenuTableViewLeading.constant = -(UIScreen.main.bounds.width)
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        handleDimmedBackgroundLayerViewVisibilityForMenu(isHidden: !isVisible)
    }
    
    func handleDimmedBackgroundLayerViewVisibilityForMenu(isHidden: Bool) {
        UIView.transition(with: self.dimmedBackgroundLayerView, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.dimmedBackgroundLayerView.isHidden = isHidden
        }, completion: nil)
    }
    
    
    // MARK: - Menu Options
    @IBAction func userProfileButtonPressed(_ sender: UIButton) {
        if !AuthenticationManager.isUserAuthenticated {
            self.openSignInViewController()
        }
        else {
            self.performSegue(segueID: "UserProfileTVCSegue")
            //UserProfileSegue
            //UserProfileTVCSegue
        }
    }
    
    @IBAction func listMySpaceButtonPressed(_ sender: UIButton) {
        //performSegue(segueID: "ListSpaceSegue")
        self.handleMenuViewVisibility(isVisible: false)
        self.presentDismissableAlertWithMessage(title: AlertMessages.HI_TITLE, message: AlertMessages.HOME_LIST_MYSPACE_ALERT_MESSAGE)
    }
}


extension HomeViewController: UITableViewDataSource, UITableViewDelegate {

    
    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == homeTableView {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == homeTableView {
            if section == 1 {
                let header = tableView.dequeueReusableCell(withIdentifier: "SearchHeaderCell")
                
                (header?.viewWithTag(1) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                (header?.viewWithTag(2) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                let btnSearch = header?.viewWithTag(3) as! UIButton
                btnSearch.layer.cornerRadius = 5
                btnSearch.clipsToBounds = true
                btnSearch.addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                (header?.viewWithTag(1111) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                (header?.viewWithTag(2222) as! UIButton).addTarget(self, action: #selector(tableViewHeaderButtonPressed(_:)), for: .touchUpInside)
                
                
                if searchHeaderHeight == 268 {
                    (header?.viewWithTag(99))?.isHidden = false
                    
                    (header?.viewWithTag(111) as! UITextField).text = (selectedEventType != nil) ? selectedEventType?.name! : nil
                    (header?.viewWithTag(222) as! UITextField).text = (selectedLocation != nil) ? selectedLocation?.name : nil
                }
                else {
                    header?.viewWithTag(99)?.isHidden = true
                }
            
                return header
            }
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == homeTableView {
            if section == 1 {
                return CGFloat(searchHeaderHeight)
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == homeTableView {
            if section == 1 {
                return 4
            }
            else {
                return 1
            }
        }
        else if tableView == eventTypeTableView {
            return eventTypes.count
        }
        else {
            return menuOptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == homeTableView {
            return UITableViewAutomaticDimension
        }
        else if tableView == eventTypeTableView {
            return 50
        }
        else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == homeTableView {
            return UITableViewAutomaticDimension
        }
        else if tableView == eventTypeTableView {
            return 50
        }
        else {
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == homeTableView {
            
            if indexPath.section == 1 {
                
                if indexPath.row == self.HOME_TABLE_VIEW_CELLS.trainingsAndPhotoshoots {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MainEventTypeTwo", for: indexPath)
                    
                    let tapLeft = UITapGestureRecognizer.init(target: self, action: #selector(tapHandlerForEventType(_:)))
                    cell.viewWithTag(7)?.addGestureRecognizer(tapLeft)
                    let tapRight = UITapGestureRecognizer.init(target: self, action: #selector(tapHandlerForEventType(_:)))
                    cell.viewWithTag(8)?.addGestureRecognizer(tapRight)
                    
                    // PARTY
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tImageOne) as! UIImageView).image = #imageLiteral(resourceName: "img-event-party")
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tLabelOne) as! UILabel).text = "PARTY"
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tLabelOneBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-2")
                    
                    // PHOTOSHOOTS
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tImageTwo) as! UIImageView).image = #imageLiteral(resourceName: "img-event-photoshoot")
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tLabelTwo) as! UILabel).text = "PHOTOSHOOTS"
                    (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_TWO_TAGS.tLabelTwoBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-3")
                    
                    return cell
                }
                else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MainEventTypeOne", for: indexPath)
                    
                    let imageView = cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.tImage) as! UIImageView
                    let label = cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.tLabel) as! UILabel
                    
                    if indexPath.row == self.HOME_TABLE_VIEW_CELLS.meetings {   // MEETINGS
                        imageView.image = #imageLiteral(resourceName: "img-event-meeting")
                        label.text = "MEETINGS"
                        (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.tLabelBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-1")
                    }
                    else if indexPath.row == self.HOME_TABLE_VIEW_CELLS.weddings {   // WEDDINGS
                        imageView.image = #imageLiteral(resourceName: "img-event-wedding")
                        label.text = "WEDDINGS"
                        (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.tLabelBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-4")
                    }
                    else if indexPath.row == self.HOME_TABLE_VIEW_CELLS.interviews {   // INTERVIEWS
                        imageView.image = #imageLiteral(resourceName: "img-event-interview")
                        label.text = "INTERVIEWS"
                        (cell.viewWithTag(self.HOME_TABLE_VIEW_CELL_TYPE_ONE_TAGS.tLabelBackground) as! UIImageView).image = #imageLiteral(resourceName: "img-cs-1")
                    }
                    
                    return cell
                }
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderImageCell", for: indexPath)
                cell.isUserInteractionEnabled = false
                return cell
            }
        }
        else if tableView == eventTypeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventTypeCell", for: indexPath)
            let imageView = cell.viewWithTag(self.EVENT_TYPE_FILTER_CELL_TAGS.imageView) as? UIImageView
            imageView?.imageFromServerURL(urlString: eventTypes[indexPath.row].iconUrl!)
            (cell.viewWithTag(self.EVENT_TYPE_FILTER_CELL_TAGS.label) as? UILabel)?.text  = eventTypes[indexPath.row].name!
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuOptionCell", for: indexPath)
            let optionAtIndex = menuOptions[indexPath.row] as! NSDictionary
            (cell.viewWithTag(1) as! UIImageView).image = optionAtIndex.value(forKey: "image") as? UIImage
            (cell.viewWithTag(2) as! UILabel).text = optionAtIndex.value(forKey: "name") as? String
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == homeTableView {
            // TODO: Main EventTypeSelection
            
            if !(indexPath.section == 0 && indexPath.row == 0) {
                
                if indexPath.row == 0 {
                    print("HomeVC homeTableView didSelectRowAt | meeting")
                    FilterManager.spaceFilter.eventTypes.append(MetaData.retrieveEventType(with: 4))
                }
                else if indexPath.row == 1 {
                    print("HomeVC homeTableView didSelectRowAt | party or photo")
                }
                else if indexPath.row == 2 {
                    print("HomeVC homeTableView didSelectRowAt | wedding")
                    FilterManager.spaceFilter.eventTypes.append(MetaData.retrieveEventType(with: 2))
                }
                else if indexPath.row == 3 {
                    print("HomeVC homeTableView didSelectRowAt | interviews")
                    FilterManager.spaceFilter.eventTypes.append(MetaData.retrieveEventType(with: 5))
                }
                
                self.performSegue(segueID: "SpacesSegue")
            }
            
        }
        else if tableView == eventTypeTableView {
            self.selectedEventType = eventTypes[indexPath.row]
            self.handleEventTypeTableViewVisibility(isHidden: true)
        }
        else if tableView == menuTableView {
            
            if indexPath.row == 0 {
                // My Spaces
                self.performSegue(segueID: "MySpacesSegue")
            }
            else if indexPath.row == 1 {
                // My Activities
                self.performSegue(segueID: "MyActivitiesSegue")
            }
            else {
                // Log Out
                self.handleMenuViewVisibility(isVisible: false)
                AuthenticationManager.logOutAuthenticatedUser()
                self.configureHomeMenu()
            }
        }
    }
}


extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.selectedLocation = place
        //print("Place name: \(place.name)")
        //print("Place address: \(String(describing: place.formattedAddress))")
        //print("Place attributions: \(String(describing: place.attributions))")
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

